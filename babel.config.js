module.exports = function (api) {
  api.cache(true)

  return {
    presets: ["babel-preset-expo"],
    plugins: ["babel-plugin-root-import"]
  }
  // return {
  //   presets: ["babel-preset-expo"],
  //   plugins: [
  //     [
  //       "module-resolver",
  //       {
  //         alias: {
  //           "@assets": "./assets",
  //           "@components": "./src/components",
  //           "@modules": "./src/modules",
  //           "@lib": "./src/lib",
  //           "@types": "./src/types",
  //           "@constants": "./src/constants",
  //           "@animations": "./src/animations",
  //           "@api": "./src/api",
  //           "@helpers": "./src/helpers",
  //           "@navigation": "./src/navigation",
  //           "@screens": "./src/screens",
  //           "@store": "./src/store",
  //           "@src": "./src"
  //         }
  //       }
  //     ]
  //   ]
  // }
}
