import { MESSAGES_URL } from "../CONSTANTS"

export const fetchRoomMessages = async (room) => {
    try {
      const url = MESSAGES_URL + `/${room}/messages`
      let response = await fetch(url, {
        method: "GET",
        headers: {
          "Content-Type": "application/json"
        }
      })
      let data = await response
      if (data.ok) {
        const messagesObj = await data.json()
        return messagesObj
      } else {
        // return dataNEws
        return { error: "Ошибка получения данных" }
      }
    } catch (err) {
      // return dataNEws
      return { error: "Ошибка отправки данных" }
    }
}