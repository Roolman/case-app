import { FILES_URL } from "../CONSTANTS"

export const getDownloadUrl = (fileUID) => {
  return FILES_URL + `${fileUID}/download`
}
