import { NEWS_URL } from "../CONSTANTS"

export const fetchNews = async (page, rows, sources = null) => {
  try {
    const url = NEWS_URL
    let params = {
      page,
      rows,
    }
    params = sources ? { ...params, sources } : {...params, approved_only:true}
    let response = await fetch(url, {
      method: "POST",
      body: JSON.stringify(params),
      headers: {
        "Content-Type": "application/json"
      }
    })
    let data = await response
    if (data.ok) {
      const newsObj = await data.json()
      return newsObj
    } else {
      // return dataNEws
      return { error: "Ошибка получения данных" }
    }
  } catch (err) {
    // return dataNEws
    return { error: "Ошибка отправки данных" }
  }
}

export const viewPost = async (id, login = "") => {
  try {
    const url = NEWS_URL + `/${id}`
    let params = {
      login
    }
    fetch(url, {
      method: "POST",
      body: JSON.stringify(params),
      headers: {
        "Content-Type": "application/json"
      }
    })
    // let data = await response
    // if (data.ok) {
    //   // return { ok: "ok" }
    // } else {
    //   // return { error: "Ошибка получения данных" }
    // }
  } catch (err) {
    // return { error: "Ошибка отправки данных" }
  }
}