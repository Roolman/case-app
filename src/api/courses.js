import { COURSES_URL, PROFILE_API_URL } from "../CONSTANTS"

export const fetchCourses = async () => {
  try {
    const url = COURSES_URL
    let response = await fetch(url, {
      method: "GET",
      headers: {
        "Content-Type": "application/json"
      }
    })
    let data = await response
    if (data.ok) {
      const coursesObj = await data.json()
      return coursesObj
    } else {
      // return dataNEws
      return { error: "Ошибка получения данных" }
    }
  } catch (err) {
    console.log(err)
    // return dataNEws
    return { error: "Ошибка отправки данных" }
  }
}

export const applyOnCourse = async (course_id, user_hash) => {
  const APPLY_ON_COURSE_URL = COURSES_URL + `${course_id}/apply`
  const body = {}
  const response = await fetch(APPLY_ON_COURSE_URL, {
    method: "POST",
    body,
    headers: {
      Authorization: "Basic " + user_hash,
    },
  })
  let data = await response
  if (data.ok) {
    const coursesObj = await data.json()
    return coursesObj
  } else {
    // return dataNEws
    console.log(data)
    return { error: "Ошибка получения данных" }
  }
}

export const leaveCourse = async (course_id, user_hash) => {
  const LEAVE_COURSE_URL = COURSES_URL + `${course_id}/leave`
  const body = {}
  const response = await fetch(LEAVE_COURSE_URL, {
    method: "POST",
    body,
    headers: {
      Authorization: "Basic " + user_hash,
    },
  })
  let data = await response
  if (data.ok) {
    const coursesObj = await data.json()
    return coursesObj
  } else {
    // return dataNEws
    console.log(data)
    return { error: "Ошибка получения данных" }
  }
}

export const fetchUserCourses = async (user_hash) => {
  try {
    const url = PROFILE_API_URL + "/courses"
    let response = await fetch(url, {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
        Authorization: "Basic " + user_hash
      }
    })
    let data = await response
    if (data.ok) {
      const coursesObj = await data.json()
      return coursesObj
    } else {
      // return dataNEws
      return { error: "Ошибка получения данных" }
    }
  } catch (err) {
    console.log(err)
    // return dataNEws
    return { error: "Ошибка отправки данных" }
  }
}