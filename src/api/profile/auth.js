import * as base64 from "base-64"
import * as utf8 from "utf8"
import { PROFILE_API_URL } from "../../CONSTANTS"

// Функция Логина
export async function login(login, password) {
  try {
    const full_url = PROFILE_API_URL
    const utf8_info = utf8.encode(login + ":" + password)
    const hash = base64.encode(utf8_info)
    const profileResponse = await fetch(full_url, {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
         Authorization: "Basic " + hash
      },
    })
    const result = await profileResponse
    let data = {}
    if (result.ok) {
      data = await result.json()
      return { ...data, hash }
    } else {
      data = await result
      if (result.status == 404) {
        return { error: "Пользователь не найден" }
      }
      if (result.status == 401) {
        return { error: "Неверный логин или пароль" }
      } else {
        console.log(result.status)
        return { error: "Неверный логин или пароль" }
      }
    }
  } catch (err) {
    console.log(err)
    // Если что-то упало (в том числе на стороне сервера)
    return { error: "К сожалению, сервис недоступен" }
  }
}