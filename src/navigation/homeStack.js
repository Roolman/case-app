import * as React from "react"
import { createStackNavigator, HeaderBackButton } from "@react-navigation/stack"
import { useNavigation } from "@react-navigation/native"

import MainScreen from "~/src/screens/Main/MainScreen"
import ExternalSiteScreen from "~/src/screens/Main/ExternalSite/ExternalSiteScreen"
import CourseRegistrationScreen from "~/src/screens/Main/CourseRegistration/CourseRegistrationScreen"
import DocumentRequestScreen from "~/src/screens/Main/DocumentRequest/DocumentRequestScreen"

export default function HomeStack() {
  const Stack = createStackNavigator()
  const navigation = useNavigation()
  const HeaderLeft = ({ label }) => <HeaderBackButton label={" "} onPress={() => navigation.navigate("ГлавнаяСтраница")} />
  return (
    <Stack.Navigator>
      <Stack.Screen name='ГлавнаяСтраница' component={MainScreen} options={{ headerShown: false }} />
      <Stack.Screen name='ВнешнийСайт' component={ExternalSiteScreen} options={{ headerLeft: () => <HeaderLeft /> }} />
      <Stack.Screen name='РегистрацияНаКурс' component={CourseRegistrationScreen} options={{ headerLeft: () => <HeaderLeft /> }} />
      <Stack.Screen name='ЗаявкиБумаги' component={DocumentRequestScreen} options={{ headerLeft: () => <HeaderLeft /> }} />
    </Stack.Navigator>
  )
}
