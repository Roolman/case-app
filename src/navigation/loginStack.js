import * as React from "react"
import { createStackNavigator, HeaderBackButton } from "@react-navigation/stack"
import { useNavigation } from "@react-navigation/native"

import { useSelector } from "react-redux"
import { useNetInfo } from "~/src/helpers/Connection"
import { NoInternet } from "~/src/components/NoInternet"
import theme from "~/src/components/theme.style"

import PersonalScreen from "~/src/screens/Profile/Personal/PersonalScreen"
import LoginScreen from "~/src/screens/Profile/Login/LoginScreen"

const appTheme = theme.lightTheme

export default function LoginStack() {
  const state = useSelector((state) => state.auth)
  const connected = useNetInfo()
  const Stack = createStackNavigator()
  const navigation = useNavigation()
  const HeaderLeft = ({ label }) => <HeaderBackButton label={label} onPress={() => navigation.navigate("Personal")} />

  if (!connected) {
    return <NoInternet />
  }

  if (state.isLoggedIn) {
    return (
      <Stack.Navigator>
        <Stack.Screen name='Personal' component={PersonalScreen} options={{ headerShown: false }} />
      </Stack.Navigator>
    )
  }

  return (
    <Stack.Navigator>
      <Stack.Screen name='Login' component={LoginScreen} options={{ headerShown: false }} />
    </Stack.Navigator>
  )
}

const headerOptions = {
  headerTintColor: appTheme.headerTintColor
}
