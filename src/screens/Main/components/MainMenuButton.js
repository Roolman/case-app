import React, { useRef, useState } from "react"
import { Dimensions, StyleSheet, TouchableWithoutFeedback, View, Text, Animated } from "react-native"
import { useRoute, useNavigation } from "@react-navigation/native"
import { useSelector } from "react-redux"

import FontAwesome5 from "react-native-vector-icons/FontAwesome5"
import { Image } from "react-native-elements"

import {Button} from "react-native-elements"
import theme, { commonShadow } from "~/src/components/theme.style"
import { fontSizeMult } from "~/src/CONSTANTS"

const BUTTON_WIDTH = Dimensions.get("window").width * 0.32
const BUTTON_HEIGHT = Dimensions.get("window").width * 0.32

const appTheme = theme.lightTheme
const default_image = require("~/assets/images/news-image-placeholder.png") // Заменить на стандартную фотку

export default function MenuButton(params) {
  const springValue = useRef(new Animated.Value(1)).current
  const theme = useSelector((state) => state.theme)

  const route = useRoute()
  const { title, iconName, description, navigation, routeTo, webURL, img } = params
  //const {  } = route.params
  const EXTERNAL = !routeTo || webURL

  const pressInAnimate = () => {
    Animated.timing(springValue, {
      toValue: 0.9,
      duration: 250,
      useNativeDriver: true,

      delay: 0
    }).start()
  }

  const pressOutAnimate = () => {
    Animated.timing(springValue, {
      toValue: 1,
      duration: 250,
      useNativeDriver: true,
      delay: 0
    }).start()
  }

  return (
    <TouchableWithoutFeedback
      onPressIn={pressInAnimate}
      onPressOut={pressOutAnimate}
      onPress={() => (EXTERNAL ? navigation.navigate("ВнешнийСайт", { webURL, title }) : navigation.navigate(routeTo))}
    >
      <Animated.View style={{ ...styles.mainMenuButtonContainer, transform: [{ scale: springValue }] }}>
        <View style={styles.textContainer}>
          <Image
              source={img}
              containerStyle={styles.itemImageContainer}
              resizeMode={"cover"}
              style={styles.image}
          />
        </View>
      </Animated.View>
    </TouchableWithoutFeedback>
  )
}

const styles = StyleSheet.create({
  mainMenuButtonContainer: {
    backgroundColor: "#fdfffe",
    borderRadius: 18,
    marginBottom: 10,
    marginTop: 10,
    marginHorizontal: 5,
    width: BUTTON_WIDTH,
    height: BUTTON_HEIGHT,
    flexDirection: "row",
    justifyContent: "space-between",
    ...commonShadow
  },
  textContainer: {
    flexShrink: 1,
    flexDirection: "column",
    justifyContent: "space-between"
  },
  itemImageContainer: {
    borderRadius: 18,
    overflow: "hidden",
    height: BUTTON_HEIGHT,
    width: BUTTON_WIDTH
  },
  image: {
    height: BUTTON_HEIGHT,
    width: BUTTON_WIDTH
  },
})
