import React, { useRef, useState } from "react"
import { Dimensions, StyleSheet, TouchableWithoutFeedback, View, Text, Animated } from "react-native"
import { useRoute, useNavigation } from "@react-navigation/native"
import { useSelector } from "react-redux"

import FontAwesome5 from "react-native-vector-icons/FontAwesome5"
import { Image } from "react-native-elements"
import { getDownloadUrl } from "~/src/api/files"

import theme, { commonShadow } from "~/src/components/theme.style"
import { fontSizeMult, SCREEN_WIDTH } from "~/src/CONSTANTS"

const PADDING_HORIZONTAL = 10
const BUTTON_WIDTH = Dimensions.get("window").width * 0.9
const BUTTON_HEIGHT = Dimensions.get("window").height * 0.3

const appTheme = theme.lightTheme
const default_image = require("~/assets/images/news-image-placeholder.png") // Заменить на стандартную фотку

export default function CourseButton(params) {
  const springValue = useRef(new Animated.Value(1)).current
  const theme = useSelector((state) => state.theme)

  const { title, description, navigation, user, imageUID, course } = params

  const pressInAnimate = () => {
    Animated.timing(springValue, {
      toValue: 0.9,
      duration: 250,
      useNativeDriver: true,

      delay: 0
    }).start()
  }

  const pressOutAnimate = () => {
    Animated.timing(springValue, {
      toValue: 1,
      duration: 250,
      useNativeDriver: true,
      delay: 0
    }).start()
  }

  return (
    <TouchableWithoutFeedback
      onPressIn={pressInAnimate}
      onPressOut={pressOutAnimate}
      onPress={() => navigation.navigate("РегистрацияНаКурс", { title, user, course_id: course.id })}
    >
      <Animated.View style={{ ...styles.mainMenuButtonContainer, transform: [{ scale: springValue }] }}>
        <View style={styles.сardContainer}>
          <Image
            source={imageUID ? { uri: getDownloadUrl(imageUID) } : default_image}
            containerStyle={styles.itemImageContainer}
            // contentContainerStyle={styles.itemImageContentContainer}
            resizeMode={"cover"}
            // resizeMode={item.source.id == "2" && item.preview_img ? "contain" : "cover"}
            style={styles.image}
          />
          <Text style={styles.titleStyle}>{title}</Text>
          <Text style={styles.descriptionStyle} numberOfLines={3}>{description}</Text>
        </View>
      </Animated.View>
    </TouchableWithoutFeedback>
  )
}

const styles = StyleSheet.create({
  mainMenuButtonContainer: {
    backgroundColor: "#fdfffe",
    borderRadius: 24,
    marginTop: 10,
    marginBottom: 100,
    width: BUTTON_WIDTH,
    height: BUTTON_HEIGHT,
    flexDirection: "row",
    justifyContent: "space-between",
    ...commonShadow
  },
  сardContainer: {
  },
  titleStyle: {
    fontSize: 18 * fontSizeMult,
    fontWeight: "600",
    position: "absolute",
    top: 20,
    left: 10,
    right: 5,
    color: "white"
  },
  descriptionStyle: {
    paddingTop: 2,
    paddingHorizontal: 5,
    fontSize: 12 * fontSizeMult,
    color: appTheme.tabBarOptions.inactiveTintColor
  },
  itemImageContainer: {
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,
    overflow: "hidden",
    height: BUTTON_HEIGHT * 0.7,
    width: BUTTON_WIDTH
  },
  image: {
    height: BUTTON_HEIGHT * 0.7,
    width: BUTTON_WIDTH
  },
})
