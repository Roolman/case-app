import React, { useRef, useState } from "react"
import { Dimensions, StyleSheet, TouchableWithoutFeedback, View, Text, Animated } from "react-native"
import { useRoute, useNavigation } from "@react-navigation/native"
import { useSelector } from "react-redux"

import FontAwesome5 from "react-native-vector-icons/FontAwesome5"

import theme, { commonShadow } from "~/src/components/theme.style"
import { fontSizeMult } from "~/src/CONSTANTS"

const BUTTON_WIDTH = Dimensions.get("window").width * 0.5
const BUTTON_HEIGHT = Dimensions.get("window").height * 0.17

const appTheme = theme.lightTheme

export default function DocumentButton(params) {
  const springValue = useRef(new Animated.Value(1)).current
  const theme = useSelector((state) => state.theme)

  const route = useRoute()
  const { title, description, navigation, routeTo, webURL, iconName, backgroundColor } = params
  //const {  } = route.params
  const EXTERNAL = !routeTo || webURL

  const pressInAnimate = () => {
    Animated.timing(springValue, {
      toValue: 0.9,
      duration: 250,
      useNativeDriver: true,

      delay: 0
    }).start()
  }

  const pressOutAnimate = () => {
    Animated.timing(springValue, {
      toValue: 1,
      duration: 250,
      useNativeDriver: true,
      delay: 0
    }).start()
  }

  return (
    <TouchableWithoutFeedback
      onPressIn={pressInAnimate}
      onPressOut={pressOutAnimate}
      onPress={() => navigation.navigate("ЗаявкиБумаги", { webURL, title })}
    >
      <Animated.View style={{ ...styles.mainMenuButtonContainer, backgroundColor: backgroundColor, transform: [{ scale: springValue }] }}>
        <View style={styles.textContainer}>
          <View style={styles.headContainer}>
            <FontAwesome5 name={iconName} color={"white"} size={23} />
            <Text style={styles.titleStyle}>{title}</Text>
          </View>
          <Text style={styles.descriptionStyle}>{description}</Text>
        </View>
      </Animated.View>
    </TouchableWithoutFeedback>
  )
}

const styles = StyleSheet.create({
  mainMenuButtonContainer: {
    borderRadius: 24,
    marginBottom: 10,
    marginTop: 10,
    marginHorizontal: 5,
    paddingHorizontal: 20,
    paddingVertical: 15,
    width: BUTTON_WIDTH,
    height: BUTTON_HEIGHT,
    flexDirection: "row",
    justifyContent: "space-between",
    ...commonShadow
  },
  textContainer: {
    flexShrink: 1,
    flexDirection: "column",
    justifyContent: "space-between"
  },
  headContainer: {
    flexDirection: "row",
    alignItems: "center"
  },
  titleStyle: {
    paddingLeft: 8,
    fontSize: 18 * fontSizeMult,
    fontWeight: "600",
    color: "white"
  },
  descriptionStyle: {
    fontSize: 12 * fontSizeMult,
    color: "white"
  }
})
