import React, { useRef, useEffect, useState } from "react"
import { View, TouchableOpacity, StyleSheet, SafeAreaView, Text } from "react-native"

import { WebView } from "react-native-webview"
import { useRoute, useNavigation } from "@react-navigation/native"
import { HeaderBackButton } from "@react-navigation/stack"
import LottieView from "lottie-react-native"
import {Button} from "react-native-elements"

import { commonShadow } from "~/src/components/theme.style"
import { SCREEN_HEIGHT, SCREEN_WIDTH } from "~/src/CONSTANTS"
import { useSelector } from "react-redux"
import { applyOnCourse, leaveCourse, fetchUserCourses } from "~/src/api/courses"
import { fontSizeMult } from "../../../CONSTANTS"

export default function CourseRegistrationScreen() {
  const webViewRef = useRef(null)
  const theme = useSelector((state) => state.theme)
  const state = useSelector((state) => state.auth)

  const [registered, setRegistered] = useState(null)
  const [userCourses, setUserCourses] = useState([])

  const route = useRoute()
  const navigation = useNavigation()

  const { title, course_id } = route.params
  const user = state.user
  
  const getUserCourses = async () => {
    // Запрашиваем курсы
    if(state.user) {
      const courses = await fetchUserCourses(state.user.hash)
      if(courses.error) return
      setUserCourses(courses)
      if(isCourseAlreadyBeenAplied(courses)) setRegistered(true)
    } else {
      setUserCourses([])
      setRegistered(null)
    }
  }

  useEffect(() => {
    navigation.setOptions({ title })

    //getUserCourses()
  }, [])

  useEffect(() => {

    getUserCourses()

  }, [state])

  const registerOrLeaveCourse = async () => {
    if(!registered) {
      await applyOnCourse(course_id, user.token)
      setRegistered(true)
    } else {
      await leaveCourse(course_id, user.token)
      setRegistered(false)
    }
  }

  const isCourseAlreadyBeenAplied = (courses) => {
    return Boolean(courses.find(x => x.id == course_id))
  }

  if(!user) {
    return (    
    <SafeAreaView style={styles.container}>
      <LottieView
        resizeMode='cover'
        style={{ width: SCREEN_WIDTH * 0.9 }}
        source={require("~/src/animations/startSearchSasha_Bodymovin.json")}
        autoPlay
        loop={true}
      />
      <Text style={styles.loginText}>Войдите, чтобы записаться на курс</Text>
      <Button
          containerStyle={styles.buttonContainerStyle}
          buttonStyle={{ ...styles.buttonStyle, backgroundColor: theme.mainColor }}
          disabledStyle={{ ...styles.buttonStyle, backgroundColor: theme.mainColor }}
          titleStyle={styles.buttonTitleStyle}
          onPress={() => navigation.navigate("Login")}
          iconContainerStyle={styles.iconContainer}
          title={"Войти"}
      />
    </SafeAreaView>
      )
  }

  return (
    <SafeAreaView style={styles.container}>
      <LottieView
        resizeMode='cover'
        style={{ width: SCREEN_WIDTH * 0.9 }}
        source={require("~/src/animations/startSearchSasha_Bodymovin.json")}
        autoPlay
        loop={true}
      />
      <Button
          containerStyle={styles.buttonContainerStyle}
          buttonStyle={{ ...styles.buttonStyle, backgroundColor: theme.mainColor }}
          disabledStyle={{ ...styles.buttonStyle, backgroundColor: theme.mainColor }}
          titleStyle={styles.buttonTitleStyle}
          onPress={() => registerOrLeaveCourse()}
          iconContainerStyle={styles.iconContainer}
          title={registered ? "Покинуть курс" : "Зарегестрироваться"}
      />
    </SafeAreaView>
  )
}

const styles = StyleSheet.create({
  container: { flex: 1, alignItems: "center", justifyContent:"center" },
  buttonContainerStyle: {
    alignSelf: "center",
    height: SCREEN_HEIGHT / 10,
    width: SCREEN_WIDTH * 0.5,
    marginVertical: 5
  },
  buttonStyle: {
    borderRadius: SCREEN_HEIGHT / 30,
    height: SCREEN_HEIGHT / 15,
    width: SCREEN_WIDTH * 0.5
  },
  buttonTitleStyle: {
    fontSize: 14*fontSizeMult,
    color: "white",
    fontFamily: "light"
  },
  loginText: {
    fontSize: 18,
    color: "black",
    fontFamily: "light"
  }
})
