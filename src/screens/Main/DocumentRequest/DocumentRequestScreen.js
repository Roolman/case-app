import React, { useRef, useEffect, useState } from "react"
import { View, TouchableOpacity, StyleSheet, SafeAreaView } from "react-native"

import { WebView } from "react-native-webview"
import { useRoute, useNavigation } from "@react-navigation/native"
import { HeaderBackButton } from "@react-navigation/stack"
import LottieView from "lottie-react-native"
import {Button} from "react-native-elements"

import { commonShadow } from "~/src/components/theme.style"
import { SCREEN_HEIGHT, SCREEN_WIDTH } from "~/src/CONSTANTS"
import { useSelector } from "react-redux"

export default function DocumentRequestScreen() {
  const webViewRef = useRef(null)
  const theme = useSelector((state) => state.theme)

  const [requested, setRequested] = useState(null)

  const route = useRoute()
  const navigation = useNavigation()

  const { webURL, title } = route.params

  useEffect(() => {
    navigation.setOptions({ title })
  }, [])

  requestDoc = () => {
    setRequested(true)
  }

  return (
    <SafeAreaView style={styles.container}>
      <LottieView
        resizeMode='cover'
        style={{ width: SCREEN_WIDTH * 0.9 }}
        source={require("~/src/animations/attestMarksLoading_Bodymovin.json")}
        autoPlay
        loop={true}
      />
      <Button
          containerStyle={styles.buttonContainerStyle}
          buttonStyle={{ ...styles.buttonStyle, backgroundColor: theme.mainColor }}
          disabledStyle={{ ...styles.buttonStyle, backgroundColor: theme.mainColor }}
          titleStyle={styles.buttonTitleStyle}
          disabled={requested}
          onPress={() => requestDoc()}
          iconContainerStyle={styles.iconContainer}
          title={requested ? "Заявка отправлена" : "Отправить заявку"}
      />
    </SafeAreaView>
  )
}

const styles = StyleSheet.create({
  container: { flex: 1, alignItems: "center", justifyContent:"center" },
  buttonContainerStyle: {
    alignSelf: "center",
    height: SCREEN_HEIGHT / 10,
    width: SCREEN_WIDTH * 0.5,
    marginVertical: 5
  },
  buttonStyle: {
    borderRadius: SCREEN_HEIGHT / 30,
    height: SCREEN_HEIGHT / 15,
    width: SCREEN_WIDTH * 0.5
  },
  buttonTitleStyle: {
    fontSize: 18,
    color: "white",
    fontFamily: "light"
  },
})
