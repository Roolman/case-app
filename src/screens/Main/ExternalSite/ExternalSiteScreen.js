import React, { useRef, useEffect } from "react"
import { View, TouchableOpacity, StyleSheet } from "react-native"

import { WebView } from "react-native-webview"
import { useRoute, useNavigation } from "@react-navigation/native"
import { HeaderBackButton } from "@react-navigation/stack"

import theme from "~/src/components/theme.style"
import FontAwesome5 from "react-native-vector-icons/FontAwesome5"
import { SCREEN_HEIGHT, SCREEN_WIDTH } from "~/src/CONSTANTS"

const appTheme = theme.lightTheme

export default function ExternalSiteScreen() {
  const webViewRef = useRef(null)

  const route = useRoute()
  const navigation = useNavigation()

  const { webURL, title } = route.params

  useEffect(() => {
    navigation.setOptions({ title })
  }, [])

  const backButtonHandler = () => {
    webViewRef.current.goBack()
  }

  const frontButtonHandler = () => {
    webViewRef.current.goForward()
  }

  return (
    <View style={styles.container}>
      <WebView
        source={{ uri: webURL }}
        style={styles.webview}
        ref={webViewRef}
        javaScriptEnabled={true}
        domStorageEnabled={true}
        startInLoadingState={false}
      />

      <View style={styles.controlPanel}>
        <TouchableOpacity style={styles.backButton} onPress={backButtonHandler}>
          <FontAwesome5 name={"chevron-left"} color={appTheme.tabBarOptions.inactiveTintColor} size={SCREEN_HEIGHT / 22} style={styles.buttonIcon} />
        </TouchableOpacity>

        <TouchableOpacity style={styles.frontButton} onPress={frontButtonHandler}>
          <FontAwesome5 name={"chevron-right"} color={appTheme.tabBarOptions.inactiveTintColor} size={SCREEN_HEIGHT / 22} style={styles.buttonIcon} />
        </TouchableOpacity>
      </View>
    </View>
  )
}

const styles = StyleSheet.create({
  container: { flex: 1 },
  webview: { height: SCREEN_HEIGHT * 0.8 },
  controlPanel: {
    alignItems: "flex-start",
    justifyContent: "flex-end",
    flexDirection: "row",
    height: SCREEN_HEIGHT / 15,
    backgroundColor: appTheme.tabTheme.colors.background
  },
  backButton: { marginRight: SCREEN_WIDTH * 0.25 },
  buttonIcon: { padding: 5 },
  frontButton: { marginRight: SCREEN_WIDTH * 0.55 }
})
