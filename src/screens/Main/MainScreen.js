import React, { useRef, useState, useEffect } from "react"
import { StyleSheet, Image, SafeAreaView } from "react-native"
import { Image as Img} from "react-native-elements"
import { Platform } from "react-native"

import { FlatList, TouchableWithoutFeedback } from "react-native-gesture-handler"
import { useSelector } from "react-redux"
import { Text, View, Animated } from "react-native"
import { useNavigation } from "@react-navigation/native"
import Constants from "expo-constants"

import MenuButton from "./components/MainMenuButton"
import DocumentButton from "./components/DocumentButton"
import CourseButton from "./components/CourseButton"
import Carousel from 'react-native-snap-carousel'
import { useNetInfo } from "~/src/helpers/Connection"
import { NoInternet } from "~/src/components/NoInternet"
import { SCREEN_HEIGHT, SCREEN_WIDTH, fontSizeMult } from "~/src/CONSTANTS"
import WebviewModalize from "~/src/components/WebviewModalize/WebviewModalize"
import theme, { commonShadow } from "~/src/components/theme.style"
import { fetchCourses } from "~/src/api/courses"

const appTheme = theme.lightTheme
const isAndroid = Platform.OS === "android"

const TOP_STROIES = [
  {
    key: "1",
    title: "Как работает АЭС?",
    description: "Короткое описание о чем-то",
    img: require('~/assets/images/story-1.png'),
    iconName: "home",
    url: "https://www.rosatom.ru/about-nuclear-industry/powerplant/"
  },
  {
    key: "2",
    title: "Росатом - работодатель года",
    description: "Короткое описание о чем-то",
    img: require('~/assets/images/story-2.png'),
    iconName: "home",
    url: "https://www.rosatom.ru/journalist/arkhiv-novostey/rosatom-vozglavil-reyting-luchshikh-rabotodateley-rossii-po-versii-kadrovoy-platformy-headhunter/"
  },
  {
    key: "3",
    title: "Устойчивое развитие",
    description: "Короткое описание о чем-то",
    img: require('~/assets/images/story-3.png'),
    iconName: "home",
    url: "https://www.rosatom.ru/sustainability/"
  },
  {
    key: "4",
    title: "Карьерные возможности",
    description: "Короткое описание о чем-то",
    img: require('~/assets/images/story-4.png'),
    iconName: "home",
    url: "https://spb.hh.ru/employer/577743"
  },
  {
    key: "5",
    title: "Новые направления деятельности",
    description: "Короткое описание о чем-то",
    img: require('~/assets/images/story-5.png'),
    iconName: "home",
    url: "https://www.rosatom.ru/about/mission/"
  },
]

const DOCUMENTS = [
  {
    key: "1",
    title: "Пропуск",
    description: "Получите личный электронный пропуск",
    imageUrl: "",
    backgroundColor: "#1a7ae8",
    iconName: "id-badge"    
  },
  {
    key: "2",
    title: "НДФЛ-2",
    description: "Подайте заявку на отчетную форму",
    imageUrl: "",
    backgroundColor: "green",
    iconName: "sticky-note"    
  },
  {
    key: "3",
    title: "Отпуск",
    description: "Заявление на отпуск",
    imageUrl: "",
    backgroundColor: "#e8da1a",
    iconName: "plane-departure"    
  },
  {
    key: "4",
    title: "Больничный",
    description: "Сообщите о необходимости получить больничный лист",
    imageUrl: "",
    backgroundColor: "purple",
    iconName: "notes-medical"    
  },
]

const COURSES = [
  {
    key: "1",
    title: "Курс 1",
    description: "Станьте лучше, получите новые компетенции Станьте лучше, получите новые компетенции Станьте лучше, получите новые компетенции Станьте лучше, получите новые компетенции Станьте лучше, получите новые компетенции",
    imageUrl: ""
  },
  {
    key: "2",
    title: "Продвинутый Excel",
    description: "Станьте гуру экселя",
    imageUrl: ""
  },
  {
    key: "3",
    title: "Курс 3",
    description: "Станьте лучше, получите новые компетенции",
    imageUrl: ""
  },
  {
    key: "4",
    title: "Курс 4",
    description: "Станьте лучше, получите новые компетенции",
    imageUrl: ""
  },
  {
    key: "5",
    title: "Курс 5",
    description: "Станьте лучше, получите новые компетенции",
    imageUrl: ""
  },
]

export default function MainScreen() {
  const state = useSelector((state) => state.auth)
  const theme = useSelector((state) => state.theme)

  const connected = useNetInfo()
  const navigation = useNavigation()

  const articleTransform = useRef(new Animated.Value(1)).current
  const modalizeRef = useRef(null)
  const [modalURI, setModalURI] = useState(null)

  const topStoriesFlatListRef = useRef(null)
  const documentsFlatListRef = useRef(null)
  const carouselRef = useRef(null)

  const [courses, setCourses] = useState([{key: "1", title: "", decription: ""}])


  const [isLecturerOrWorker, setIsLecturerOrWorker] = useState(state.user && (state.user.type == "Преподаватель" || state.user.type == "Сотрудник"))

  let scrollY = useRef(new Animated.Value(0)).current

  const pressInAnimate = () => {
    Animated.timing(articleTransform, {
      toValue: 0.9,
      duration: 250,
      useNativeDriver: true
    }).start()
  }

  const pressOutAnimate = () => {
    Animated.timing(articleTransform, {
      toValue: 1,
      duration: 250,
      useNativeDriver: true
    }).start()
  }

  const onWebviewClose = () => {
    scrollY.setValue(0)
  }
  const onWebviewOpen = () => {
    scrollY.setValue(70)
  }

  const renderTopStory = ({ item, index, separators }) => {
    return (
      <MenuButton 
        title={item.title}
        img={item.img}
        navigation={navigation} 
        description={item.description}
        webURL={item.url}
        iconName={item.iconName}
      />
    )
  }

  const renderDocs = ({ item, index, separators }) => {
    return (
      <DocumentButton 
        title={item.title}
        navigation={navigation} 
        description={item.description}
        backgroundColor={item.backgroundColor}
        iconName={item.iconName}
      />
    )
  }

  const renderCourse = ({ item, index, separators }) => {
    return (
      <CourseButton 
        title={item.title}
        navigation={navigation} 
        description={item.description}
        imageUID={item.file_uuid}
        course={item}
        user={state.user}
      />
    )
  }

  // Определяем препод или студент
  useEffect(() => {
    if (state.user) {
      const lecturerOrWorker = state.user.type == "Преподаватель" || state.user.type == "Сотрудник"
      setIsLecturerOrWorker(lecturerOrWorker)
    }else{
      setIsLecturerOrWorker(null)
    }
  }, [state])

  // Получаем курсы при старте
  useEffect(() => {
    const getCourses = async () => {
      const courses = await fetchCourses()
      console.log(courses)
      if(courses.error) return
      const componentCourses = courses.map((c, i) => {
        return {...c, key: c.id}
      })
      setCourses(componentCourses)
    }

    getCourses()
  }, [])

  if (!connected) {
    return <NoInternet />
  }

  return (
    <SafeAreaView style={styles.mainView}>
        <FlatList
          ref={topStoriesFlatListRef}
          decelerationRate={"fast"}
          data={TOP_STROIES}
          showsVerticalScrollIndicator={false}
          showsHorizontalScrollIndicator={false}
          horizontal={true}
          renderItem={renderTopStory}
          contentContainerStyle={styles.topStoriesFlatListContainer}
          style={styles.topStoriesFlatListStyle}
        />
        <Text style={styles.headerTitle}>Заявки</Text>
        <FlatList
          ref={documentsFlatListRef}
          decelerationRate={"fast"}
          data={DOCUMENTS}
          showsVerticalScrollIndicator={false}
          showsHorizontalScrollIndicator={false}
          horizontal={true}
          renderItem={renderDocs}
          contentContainerStyle={styles.documentsFlatListContainer}
          style={styles.documentsFlatListStyle}
        />
        <Text style={styles.headerTitle}>Цифровые компетенции</Text>
        <Carousel
          ref={carouselRef}
          data={courses}
          renderItem={renderCourse}
          sliderWidth={SCREEN_WIDTH}
          itemWidth={SCREEN_WIDTH*0.9}
          layout={'tinder'} 
        />
      <WebviewModalize modalizeRef={modalizeRef} modalURI={modalURI} modalHeight={0.8 * SCREEN_HEIGHT} onClose={onWebviewClose} />
    </SafeAreaView>
  )
}

const styles = StyleSheet.create({
  mainView: {
    justifyContent: "center",
    paddingTop: isAndroid ? 30 : 0
  },
  headerTitle: {
    fontSize: 20 * fontSizeMult,
    fontWeight: "600",
    paddingLeft: 20,
    paddingTop: 20 * fontSizeMult
  },
  topStoriesFlatListStyle: {
  },
  topStoriesFlatListContainer: {
    alignItems: "center",
  },
  documentsFlatListStyle: {
  },
  documentsFlatListContainer: {
    alignItems: "center"
  },
})
