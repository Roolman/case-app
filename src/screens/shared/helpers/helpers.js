import { Platform, StyleSheet } from "react-native"

import theme, { commonShadow } from "~/src/components/theme.style"
import { SCREEN_WIDTH, SCREEN_HEIGHT, fontSizeMult } from "~/src/CONSTANTS"

const appTheme = theme.lightTheme
const isAndroid = Platform.OS == "android"

export const commonStyles = StyleSheet.create({
  emptyComponentText: {
    fontSize: 14,
    color: appTheme.tabBarOptions.inactiveTintColor,
    textAlign: "center"
  },
  loadingView: {
    flex: 1,
    alignSelf: "center",
    justifyContent: "center",
    alignItems: "center"
  },
  loadingAnimation: {
    height: 300,
    width: 300
  },
  notFoundView: { alignSelf: "center" },
  notFoundAnimation: {
    height: SCREEN_WIDTH * 0.7,
    width: SCREEN_WIDTH * 0.7
  },
  container: { flex: 1, paddingTop: isAndroid ? 20 : 0 },
  mainView: { flex: SCREEN_HEIGHT >= 700 ? 0.13 : 0.2, flexDirection: "row", justifyContent: "space-around" },
  optionContainer: {
    backgroundColor: "white",
    width: 0.9 * SCREEN_WIDTH,
    alignSelf: "center"
  },
  optionText: {
    fontSize: 20,
    borderWidth: 0,
    borderColor: "white"
  },
  cancelContainer: {
    backgroundColor: "white",
    width: 0.9 * SCREEN_WIDTH,
    alignSelf: "center",
    borderRadius: 5
  },
  cancelText: {
    fontSize: 20,
    color: "red"
  },
  button: {
    backgroundColor: appTheme.tabBarOptions.activeTintColor,
    ...commonShadow,
    width: 0.4 * SCREEN_WIDTH,
    borderRadius: 20,
    alignSelf: "center",
    padding: 5,
    marginBottom: 4,
    marginHorizontal: 4
  },
  buttonTitle: {
    fontSize: 14 * fontSizeMult
  },
  listContainer: {
    borderBottomColor: appTheme.tabBarOptions.activeTintColor,
    borderBottomWidth: 1,
    margin: 0,
    marginTop: 5
  },
  itemContainer: {
    backgroundColor: "rgba(255, 255, 255, 1)",
    width: SCREEN_WIDTH,
    height: 70 * fontSizeMult,
    justifyContent: "center",
    marginBottom: 10,
    flexDirection: "column",
    ...commonShadow
  },
  itemSubjectContainer: {
    paddingLeft: 10,
    alignItems: "flex-start",
    flexDirection: "row"
  },
  itemSubjectText: { fontWeight: "bold", fontSize: 12 * fontSizeMult },
  itemInfoContainer: { flexDirection: "row", justifyContent: "space-between", alignItems: "center", paddingLeft: 10 },
  itemMainInfoText: {
    fontFamily: "regular",
    fontSize: 12 * fontSizeMult,
    color: appTheme.tabBarOptions.inactiveTintColor
  },
  itemButtonTitle: {
    fontSize: 12 * fontSizeMult,
    fontFamily: "regular",
    alignItems: "center",
    justifyContent: "center"
  },
  itemButton: {
    marginRight: 10,
    padding: 2,
    width: SCREEN_WIDTH * 0.225,
    borderRadius: 20
  }
})
