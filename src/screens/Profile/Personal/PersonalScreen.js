import React, { useState, useEffect, useRef } from "react"
import { StyleSheet, Text, View, SafeAreaView, ScrollView, Alert, LogBox, Image as Img, Animated, Platform } from "react-native"

import { Modalize } from "react-native-modalize"
import "react-native-console-time-polyfill"
import { useDispatch, useSelector } from "react-redux"
import { Image, Button } from "react-native-elements"
import { Placeholder, PlaceholderMedia, Shine } from "rn-placeholder"
import FontAwesome5 from "react-native-vector-icons/FontAwesome5"
import * as Progress from 'react-native-progress'
import QRCode from "~/src/components/react-native-qrcode-svg"
import LottieView from "lottie-react-native"
import Carousel from 'react-native-snap-carousel'
import { useNavigation, useIsFocused } from "@react-navigation/native"

import CourseButton from "./components/CourseButton"
import theme, { commonShadow } from "~/src/components/theme.style"
import { handleLogout } from "~/src/store/actions/auth"
import { SCREEN_HEIGHT, SCREEN_WIDTH, APP_KEY, fontSizeMult } from "~/src/CONSTANTS"
import { TouchableOpacity, FlatList } from "react-native-gesture-handler"
import { fetchUserCourses } from "~/src/api/courses"

import { ProgressChart } from "react-native-chart-kit"

const default_profile = require("~/assets/images/default_profile.png")
const appTheme = theme.lightTheme
//const it_center_logo = require("~/src/assets/images/Mining_new.png")
const isAndroid = Platform.OS === "android"

LogBox.ignoreLogs([
  "VirtualizedLists should never be nested" // TODO: Remove when fixed
])

const ACHIEVMENTS = [
  {
    img: require('~/assets/images/medal.png'),
  },
  {
    img: require('~/assets/images/goal.png'),
  },
  {
    img: require('~/assets/images/achievement.png'),
  },
  {
    img: require('~/assets/images/trophy.png'),
  }
]

const QRcodeURI = "https://www.rosatom.ru/career/soiskatelyam/zaveduyushchiy-byuro-propuskov-ao-atom-okhrana-g-moskva/"

export default function PersonalSreen() {
  const [loading, setLoading] = useState(true)

  const state = useSelector((state) => state.auth)
  const theme = useSelector((state) => state.theme)
  const isFocused = useIsFocused()

  const ProgressChartData = {
    labels: ["Excel", "Python", "НС"], // optional
    data: [Math.random(), Math.random(), Math.random()]
  }
  const chartConfig = {
    backgroundGradientFrom: "#EFEFEF", // appTheme.tabTheme.colors.background
    backgroundGradientFromOpacity: 1,
    backgroundGradientTo: "#EFEFEF",
    backgroundGradientToOpacity: 1,
    color: (opacity = 1) => `rgba(19, 93, 130, ${opacity})`,
  }

  const progress = Math.random()
  const progressColor = progress > 0.7 ? "green" : progress > 0.35 ? "#e8da1a" : "red"
  const day = Math.round(90*progress)

  const dispatch = useDispatch()
  const navigation = useNavigation()

  const [userCourses, setUserCourses] = useState([{key:"-1",title:"",description:""}])

  const modalizeRef = useRef(null)
  
  const getUserCourses = async () => {
    // Запрашиваем курсы
    if(state.user) {
      const courses = await fetchUserCourses(state.user.hash)
      if(courses.error) return
      setUserCourses(courses)
    } else {
      setUserCourses([])
    }
  }  

  useEffect(() => {
    const init = async () => {
      setLoading(false)
    }
    init()
  }, [])

  useEffect(() => {
    if(isFocused) getUserCourses()
  }, [isFocused])

  const getFontSizeByName = (firstname, lastname) => {
    // 45 % of screen for firstname and lastname
    const N = Math.max(firstname?.length, lastname?.length)
    const newFont = Math.round(1.33 * SCREEN_WIDTH * (50 / (N * 100)))
    if (newFont > 20) {
      return 20
    }
    if (newFont < 12) {
      return 12
    }
    return newFont
  }

  const logout = () => {
    Alert.alert(
      "Подтверждение",
      "Вы действительно хотите выйти?",
      [
        {
          text: "Нет",
          onPress: () => {},
          style: "cancel"
        },
        {
          text: "Да",
          onPress: () => dispatch(handleLogout())
        }
      ],
      { cancelable: false }
    )
  }

  const renderAchievment = ({ item }) => {
    return (<Image
      source={item.img}
      containerStyle={styles.achievImageContainer}
      resizeMethod='scale'
      resizeMode='cover'
      style={styles.achievImage}
    />)
  }

  const renderCourse = ({ item }) => {
    return (
      <CourseButton 
        title={item.title}
        navigation={navigation} 
        description={item.description}
        imageUID={item.file_uuid}
      />
    )
  }

  if (loading || !state.user) {
    // Если юзер не залогинился (или пока данные не подсосались), то пустой экран (между переходами экранов)
    return <View style={styles.emptyscreen}></View>
  }

  if (state.user) {
    const firstname = state.user.first_name
    const lastname = state.user.last_name
    const userPhotoURI = state.user.photo_uuid

    return (
      <SafeAreaView>
        <ScrollView contentContainerStyle={styles.scrollContainer} showsHorizontalScrollIndicator={false} showsVerticalScrollIndicator={false}>
        <Button 
          containerStyle={styles.selectButtonContainer}
          buttonStyle={styles.selectButton}
          titleStyle={styles.selectButtonText}
          title='Выйти'
          onPress={logout}
        />
        <View style={styles.headerContainer}>
            <View style={!isAndroid && userPhotoURI ? { ...commonShadow } : {}}>
              <Image
                source={userPhotoURI ? { uri: userPhotoURI } : default_profile}
                PlaceholderContent={
                  <Placeholder Animation={Shine}>
                    <PlaceholderMedia style={styles.imageSize} />
                  </Placeholder>
                }
                containerStyle={userPhotoURI ? { ...styles.imageContainer, ...commonShadow } : styles.imageContainer}
                resizeMethod='scale'
                resizeMode='cover'
                style={[styles.imageSize, styles.image]}
              />
            </View>
            <View style={styles.userInfoContainer}>
              <Text style={{ ...styles.userNameText, fontSize: getFontSizeByName(firstname, lastname) * fontSizeMult }}>
                {firstname} {"\n"}
                {lastname}
              </Text>
              <Text style={styles.userInfoMainText}>
                {"стажер"}
              </Text>
            </View>
            <TouchableOpacity style={{ alignItems: "center" }} activeOpacity={0.6} onPress={() => modalizeRef.current.open()}>
              <LottieView
                resizeMode='cover'
                style={styles.qrCode}
                source={require("~/src/animations/qrcode_Bodymovin.json")}
                autoPlay
                loop={false}
              />
              <Text style={styles.passText}>Пропуск</Text>
            </TouchableOpacity>
        </View>
        <View style={styles.achievmentsContainer}>
          <Text style={styles.headerTitle}>Прогресс</Text>
          <View style={styles.progressInfo}>
            <View style={styles.progressBarContainer}>
              <Text style={styles.progressText}>Испытательный срок. День {day}</Text>
              <Progress.Bar progress={progress} width={SCREEN_WIDTH*0.75} height={26} color={progressColor} borderRadius={15} />
            </View>
            <View style={styles.progressBarContainer}>
              <Text style={styles.progressText}>Цифровые компетенции</Text>
              <ProgressChart
                data={ProgressChartData}
                width={SCREEN_WIDTH * 0.84}
                height={220}
                strokeWidth={16}
                radius={32}
                chartConfig={chartConfig}
                hideLegend={false}
              />
            </View>
          </View>
          <Text style={styles.headerTitle}>Достижения</Text>
          <FlatList
            decelerationRate={"fast"}
            data={ACHIEVMENTS}
            showsVerticalScrollIndicator={false}
            showsHorizontalScrollIndicator={false}
            horizontal={true}
            renderItem={renderAchievment}
            contentContainerStyle={styles.achievContainer}
            style={styles.achievFlatListStyle}
          />
          <Text style={styles.headerTitle}>Мои курсы</Text>
          {userCourses[0]?.key != "-1" && (
            <Carousel
              data={userCourses}
              renderItem={renderCourse}
              sliderWidth={SCREEN_WIDTH}
              itemWidth={SCREEN_WIDTH*0.9}
              layout={'tinder'} 
            />
          )}      
        </View>

        </ScrollView>
        <Modalize
          ref={modalizeRef}
          modalHeight={0.6 * SCREEN_HEIGHT}
          // NOTE: быстрый фикс бага с overlay внизу модала
          modalStyle={{ top: 0.05 * SCREEN_HEIGHT }}
        >
          <View style={styles.qrCodeContainer}>
            <QRCode
              size={0.8 * SCREEN_WIDTH}
              value={QRcodeURI}
              // logo={it_center_logo}
              // logoSize={45}
              // logoMargin={0}
              // logoBackgroundColor='white'
              color={theme.mainColor}
            />
            <Text style={styles.qrCodeText}>Покажите пропуск на КПП</Text>
          </View>
        </Modalize>
      </SafeAreaView>
    )
  }
}

const styles = StyleSheet.create({
  emptyscreen: { flex: 1, alignSelf: "center", justifyContent: "center" },
  container: {
    backgroundColor: appTheme.tabTheme.colors.background,
    flex: 1
  },
  scrollContainer: {
    paddingBottom: 0
  },
  selectButton: {
    width: 60,
    backgroundColor: "transparent"
  },
  selectButtonText: {
    color: "black",
    fontSize: 12
  },
  headerContainer: {
    flexDirection: "row",
    paddingTop: 0,
    width: 0.9 * SCREEN_WIDTH,
    alignItems: "center",
    alignSelf: "center"
  },
  qrCodeContainer: {
    paddingTop: 0.08 * SCREEN_HEIGHT,
    alignItems: "center"
  },
  qrCode: {
    paddingLeft: 10,
    height: 40 * fontSizeMult,
    width: 40 * fontSizeMult
  },
  passText: {     
    paddingLeft: 40,
    fontSize: 8 * fontSizeMult, 
    color: appTheme.tabBarOptions.inactiveTintColor 
  },
  userInfoContainer: { flexDirection: "column", justifyContent: "space-between", paddingLeft: 20 },
  userNameText: {
    color: appTheme.headerText.color,
    textAlign: "auto",
    fontFamily: "bold"
  },
  userInfoMainText: {
    fontSize: 12 * fontSizeMult,
    color: appTheme.headerText.color,
    textAlign: "left",
    fontFamily: "regular"
  },
  userInfoSecondText: {
    fontSize: 10 * fontSizeMult,
    color: appTheme.tabBarOptions.inactiveTintColor,
    textAlign: "left",
    fontFamily: "regular",
    maxWidth: 0.65 * SCREEN_WIDTH,
    overflow: "hidden"
  },
  imageSize: {
    width: 0.25 * SCREEN_WIDTH,
    height: 0.25 * SCREEN_WIDTH
  },
  imageContainer: {
    borderRadius: 8
  },
  image: { overflow: "hidden" },
  headerTitle: {
    fontSize: 20 * fontSizeMult,
    fontWeight: "600",
    paddingTop: 30,
    paddingBottom: 10,
    paddingLeft: 20
  },
  achievmentsContainer: {
  },
  progressInfo: {
    paddingHorizontal: 20,
  },
  progressBarContainer: {
    marginTop: 7,
    padding: 18,
    backgroundColor: "#EFEFEF",
    borderRadius: 15,
  },
  progressText: {
    color: appTheme.tabBarOptions.inactiveTintColor,
    paddingBottom: 10
  },
  achievContainer: {
    paddingTop: 20,
    alignItems: "center",
    justifyContent: "center"
  },
  achievImageContainer: {
    paddingHorizontal: 20,
    ...commonShadow
  },
  achievImage: {
    height: 90,
    width: 90,
  }
})
