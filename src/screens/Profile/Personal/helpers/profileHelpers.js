export const getContactItemData = (item, loading = false) => {
  let innerPhone = ""
  let outerPhone = ""
  let name = ""
  if (!loading) {
    let names = item.name.split(" ")
    let [secondName, firstName, surName] = names
    if (surName && firstName) {
      name = secondName + " " + firstName[0] + ". " + surName[0] + "."
    } else if (secondName && firstName) {
      name = secondName + " " + firstName[0] + "."
    } else if (secondName) {
      name = secondName
    } else {
      name = firstName
    }
    let innerPhones = item.phones.filter((x) => x.internal)
    innerPhone = innerPhones[0] ? innerPhones[0].number : ""
    let outerPhones = item.phones.filter((x) => !x.internal)
    outerPhone = outerPhones[0] ? outerPhones[0].number : ""
  }
  let mainItem = name
  if (!name && item.job) {
    let jobWords = item.job.split(" ")
    if (jobWords.length > 2) {
      mainItem = jobWords[0] + " " + jobWords[1] + "..."
    } else {
      mainItem = item.job
    }
  } else if (!name && item.department) {
    let depWords = item.department.split(" ")
    if (depWords.length > 2) {
      mainItem = depWords[0] + " " + depWords[1] + "..."
    } else {
      mainItem = item.department
    }
  }

  return { innerPhone, outerPhone, mainItem, name }
}
