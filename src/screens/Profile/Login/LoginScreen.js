import React, { useState, useRef, useEffect } from "react"
import {
  Text,
  View,
  SafeAreaView,
  Image,
  Easing,
  Animated,
  KeyboardAvoidingView,
  StyleSheet,
  Keyboard,
  TouchableWithoutFeedback,
  Alert,
  Linking,
  ScrollView,
  Platform
} from "react-native"

import { useDispatch } from "react-redux"
import LottieView from "lottie-react-native"
import { Modalize } from "react-native-modalize"
import { Input, Button } from "react-native-elements"
import { useSelector } from "react-redux"

import theme, { commonShadow } from "~/src/components/theme.style"
import * as api from "~/src/api/profile/auth"
import { handleLogin } from "~/src/store/actions/auth"
import { SCREEN_HEIGHT, SCREEN_WIDTH, fontSizeMult } from "~/src/CONSTANTS"

const Logo = require("~/assets/images/logo_rosatom.png")
const appTheme = theme.lightTheme
const initLoginResult = {
  data: null,
  error: null
}
const isAndroid = Platform.OS == "android"

export default function LoginScreen() {
  const dispatch = useDispatch()
  const theme = useSelector((state) => state.theme)

  const [login, setLogin] = useState(null)
  const [password, setPassword] = useState(null)
  const [loading, setLoading] = useState(false)
  const [errorMsg, setErrorMsg] = useState(null)
  const [startValueY] = useState(new Animated.Value(-SCREEN_HEIGHT * 0.05))
  const [transformLogo, setTransformLogo] = useState(new Animated.Value(1))

  const pwdInput = useRef()
  const modalizeRef = useRef()
  const onHelpOpen = () => modalizeRef.current?.open()

  const loginAnimationProgress = useRef(new Animated.Value(0.133)).current // start
  const loginResult = useRef(initLoginResult).current

  const onKeybordOpenLogoAnimate = () => {
    Animated.parallel([
      Animated.timing(transformLogo, {
        toValue: 0.6,
        duration: 400,
        useNativeDriver: true
      }).start(),
      Animated.timing(startValueY, {
        toValue: SCREEN_HEIGHT * 0.04,
        duration: 400,
        useNativeDriver: true
      }).start()
    ])
  }

  const onKeybordCloseLogoAnimate = () => {
    Animated.parallel([
      Animated.timing(transformLogo, {
        toValue: 1,
        duration: 400,
        useNativeDriver: true
      }).start(),
      Animated.timing(startValueY, {
        toValue: -SCREEN_HEIGHT * 0.04,
        duration: 400,
        useNativeDriver: true
      }).start()
    ])
  }

  const loginFunc = async (login, password) => {
    if (login && password) {
      try {
        setLoading(true)
        Keyboard.dismiss()
        setErrorMsg(null)
        const result = await api.login(login, password)
        loginResult.data = result
        loginResult.error = result.error ? result.error : false
        // Если не строка, значит пришел хэш
        if (!result.error) {
          // + handle Login in animation function
          dispatch(handleLogin(loginResult.data))
        } else {
          // Если не нашелся, то выводим ошибку
          setLoading(false)
          setPassword(null)
          if (loginResult.error == "Пользователь не найден") {
            setLogin(null)
          }
        }
      } catch (error) {
        // Если что-то упало (в том числе на стороне сервера)
        setLoading(false)
        Alert.alert(
          "",
          "К сожалению, сервис недоступен",
          [
            {
              text: "OK",
              onPress: () => {}
            }
          ],
          { cancelable: false }
        )
      }
    } else {
      Alert.alert(
        "",
        "Введите логин и пароль",
        [
          {
            text: "OK",
            onPress: () => {}
          }
        ],
        { cancelable: false }
      )
    }
  }

  return (
    <SafeAreaView style={styles.container}>
      <KeyboardAvoidingView
        style={styles.keyboardavoidContainer}
        behavior='position'
        keyboardVerticalOffset={Platform.OS === "android" ? -SCREEN_HEIGHT * 0.3 : -SCREEN_HEIGHT * 0.15}
        enabled={Platform.OS === "ios"}
      >
        <ScrollView contentContainerStyle={styles.scrollContainer} keyboardShouldPersistTaps='handled'>
          <Animated.View style={{ transform: [{ scale: transformLogo }, { translateY: startValueY }] }}>
            <Image resizeMode='contain' source={Logo} style={styles.logo}></Image>
          </Animated.View>
          <View>
            <Input
              value={login}
              onChangeText={(text) => {
                try {
                  setLogin(text) // NOTE: Баг вылета приложения
                } catch (err) {}
              }}
              maxLength={100}
              placeholder='ЛОГИН'
              autoCapitalize='none'
              autoCompleteType='off'
              textContentType='none'
              importantForAutofill='no'
              keyboardType={Platform.OS === "ios" ? "ascii-capable" : "visible-password"}
              onSubmitEditing={() => pwdInput.current.focus()}
              blurOnSubmit={false}
              returnKeyType='next'
              inputStyle={[styles.inputStyle, isAndroid ? { fontWeight: "200" } : { fontFamily: "light" }]}
              inputContainerStyle={appTheme.loginInputContainerStyle}
            />

            <Input
              value={password}
              onChangeText={setPassword}
              maxLength={50}
              ref={pwdInput}
              secureTextEntry={true}
              placeholder='ПАРОЛЬ'
              autoCapitalize='none'
              autoCompleteType='off'
              textContentType='none'
              importantForAutofill='no'
              onSubmitEditing={() => {
                loginFunc(login, password)
              }}
              returnKeyType='done'
              inputStyle={[styles.inputStyle, isAndroid ? { fontWeight: "300" } : { fontFamily: "light" }]}
              inputContainerStyle={appTheme.loginInputContainerStyle}
            />
          </View>

          <Button
            containerStyle={styles.containerStyle}
            buttonStyle={{ ...styles.buttonStyle, backgroundColor: theme.mainColor }}
            disabledStyle={{ ...styles.buttonStyle, backgroundColor: theme.mainColor }}
            titleStyle={styles.buttonTitleStyle}
            disabled={loading}
            onPress={() => loginFunc(login, password)}
            iconContainerStyle={styles.iconContainer}
            icon={() =>
              (
                <View></View>
              )
            }
            title={loading ? "" : "ВХОД"}
          />
          <TouchableWithoutFeedback style={{ marginVertical: 15 }} onPress={onHelpOpen}>
            <Text style={styles.helperStyle}>Как войти</Text>
          </TouchableWithoutFeedback>
        </ScrollView>
      </KeyboardAvoidingView>

      <Modalize modalHeight={SCREEN_HEIGHT / 3} scrollViewProps={{ showsVerticalScrollIndicator: false }} ref={modalizeRef}>
        <View style={styles.helpView}>
          <Text style={styles.helpTitle}>Забыли пароль или логин?</Text>
          <Text style={styles.helpText}>
            Используйте для входа логин и пароль от lk.spmi.ru
            Или войдите, используя специальный логин demo и пароль demo
          </Text>
        </View>
      </Modalize>
    </SafeAreaView>
  )
}

const styles = StyleSheet.create({
  container: {
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "space-between",
    paddingTop: Platform.OS === "android" ? 35 : 0
  },
  scrollContainer: {
    flex: 1,
    justifyContent: "center",
    alignContent: "space-between",
    padding: SCREEN_HEIGHT * 0.05
  },
  inputStyle: {
    color: "black",
    textAlign: "center"
  },
  containerStyle: {
    alignSelf: "center",
    height: SCREEN_HEIGHT / 15,
    width: SCREEN_WIDTH * 0.5,
    marginVertical: 5,
    ...commonShadow
  },
  buttonStyle: {
    borderRadius: SCREEN_HEIGHT / 30,
    height: SCREEN_HEIGHT / 15,
    width: SCREEN_WIDTH * 0.5
  },
  buttonTitleStyle: {
    fontSize: 18,
    color: "white",
    fontFamily: "light"
  },
  iconContainer: {},
  animation: {
    marginBottom: SCREEN_HEIGHT / 30 + (0.9 * SCREEN_HEIGHT) / 48, // 45
    marginRight: SCREEN_WIDTH * 0.25 - (0.9 * SCREEN_HEIGHT) / 13, // 15
    height: (0.9 * SCREEN_HEIGHT) / 5,
    width: (0.9 * SCREEN_HEIGHT) / 5
  },
  logo: {
    width: SCREEN_HEIGHT / 8,
    height: SCREEN_HEIGHT / 8,
    alignSelf: "center"
  },
  helpView: {
    flexDirection: "column"
  },
  helpTitle: {
    fontSize: 18 * fontSizeMult,
    fontFamily: "bold",
    paddingVertical: 15,
    paddingHorizontal: 15
  },
  helpText: {
    fontSize: 12 * fontSizeMult,
    paddingHorizontal: 15
  },
  helperModalize: {},
  helpButtonStyle: {
    alignSelf: "center",
    height: SCREEN_HEIGHT / 15,
    borderRadius: SCREEN_HEIGHT / 30,
    width: SCREEN_WIDTH * 0.8,
    marginVertical: 15
  },
  helpButtonTextStyle: {
    fontSize: 15 * fontSizeMult
  },
  helperStyle: {
    color: appTheme.tabBarOptions.inactiveTintColor,
    fontSize: 10 * fontSizeMult,
    fontFamily: "bold",
    alignSelf: "center",
    marginVertical: 10
  }
})
