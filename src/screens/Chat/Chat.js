import React, { useRef, useState, useEffect, useCallback } from "react"
import { Dimensions, StyleSheet, TouchableWithoutFeedback, View, Text, Animated, ActivityIndicator } from "react-native"
import { useSelector } from "react-redux"
import { GiftedChat, Send } from 'react-native-gifted-chat'

import FontAwesome5 from "react-native-vector-icons/FontAwesome5"
import { Image } from "react-native-elements"
import { useRoute, useNavigation, useParams } from "@react-navigation/native"
import { HeaderBackButton } from "@react-navigation/stack"

import { fetchRoomMessages } from "~/src/api/messages"


export default function Chat() {
  const springValue = useRef(new Animated.Value(1)).current
  const state = useSelector((state) => state.auth)
  const theme = useSelector((state) => state.theme)
  const navigation = useNavigation()
  const HeaderLeft = ({ label }) => <HeaderBackButton label={label} onPress={() => navigation.goBack()} />

  const route = useRoute()
  const { name, title, WS  } = route.params
  
  const [messages, setMessages] = useState([])
  const [loadingMessages, setLoadingMessages] = useState(true)

  const mapMessage = (msg, isFromServer) => {
    return {
      _id: Math.round(Math.random()*1000000), // NOTE: Пофиксить
      text: msg.message,
      createdAt: isFromServer ? new Date(msg.timestamp) : new Date(),
      user: {
        _id: msg.user_id,
        name: msg.first_name + " " + msg.last_name,
        avatar: ''
      }
    }
  }

  useEffect(() => {
    
    WS.onmessage = function(evt) {
      if(evt){
        const msg = JSON.parse(evt.data)
        console.log("onmessage",msg)
        if (typeof msg != 'object') return
        if (msg.user_id == state.user.login) return
        const message = mapMessage(msg, false)
        setMessages(previousMessages => GiftedChat.append(previousMessages, [message]))
      }
    }

    // return () => WS.close()
  }, [])

  // Get messages
  useEffect(() => {
    navigation.setOptions({ title: title, headerLeft: () => <HeaderLeft label='Назад' /> })

    const getMessages = async () => {
      setLoadingMessages(true)
      const messages = await fetchRoomMessages(name)
      if(messages.error) return
      const msgs = messages.map(m => mapMessage(m, true)).sort( (a, b) => b.createdAt - a.createdAt )
      setMessages(previousMessages => GiftedChat.append(previousMessages, msgs))
      setLoadingMessages(false)
      //setTimeout(() => setLoadingMessages(false), 2000)
    }

    getMessages()
  }, [])


  const onSend = useCallback((messages = []) => {
    setMessages(previousMessages => GiftedChat.append(previousMessages, messages))
    const msg = messages[0].text
    const message = { 
      first_name: state.user.first_name, 
      last_name: state.user.last_name, 
      message: msg,
      user_id: state.user.login
    }
    WS.send(JSON.stringify(message))
  }, [])

  const renderSend = (props) => {
    return (
        <Send
            {...props}
        >
            <View style={{marginRight: 10, paddingBottom: 15}}>
                <Text style={{fontSize: 18, fontWeight: "500", color: "blue"}}>Отправить</Text>
            </View>
        </Send>
    );
  }

  return (
    <GiftedChat
      messages={messages}
      onSend={messages => onSend(messages)}
      user={{
        _id: state.user.login,
      }}
      isLoadingEarlier={true}
      placeholder={"Введите сообщение"}
      renderLoading={() => (<ActivityIndicator loading={true} size={"large"} color="#0000ff"/>)}
      renderSend={renderSend}
    />
  )
}

const styles = StyleSheet.create({

})
