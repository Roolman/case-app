import React, { Component, useEffect, useRef } from "react"
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  SafeAreaView,
  ScrollView,
  Alert,
  Image,
  FlatList,
  Animated,
  Easing,
  Linking,
  ActivityIndicator,
} from "react-native"

import Modal from "react-native-modal"
import LottieView from "lottie-react-native"
import { Modalize } from "react-native-modalize"
import { useNavigation, useIsFocused } from "@react-navigation/native"
import { useDispatch, useSelector } from "react-redux"
import { SearchBar, Button } from "react-native-elements"
import FontAwesome5 from "react-native-vector-icons/FontAwesome5"

import theme, { appBackgroundColor, commonShadow } from "~/src/components/theme.style"
import { NoInternet } from "~/src/components/NoInternet"
import { useNetInfo } from "~/src/helpers/Connection"

import MenuButton from "~/src/screens/Main/components/MainMenuButton"
const isAndroid = Platform.OS === "android"


import _ from "lodash"
import { fontSizeMult, SCREEN_HEIGHT, SCREEN_WIDTH } from "../../CONSTANTS"

const TOP_STROIES = [
  {
    key: "1",
    title: "Чемпионат AtomSkills",
    description: "Короткое описание о чем-то",
    img: require('~/assets/images/FAQ-1.png'),
    iconName: "home",
    url: "https://atomskills2020.ru"
  },
  {
    key: "2",
    title: "Кадровая политика",
    description: "Короткое описание о чем-то",
    img: require('~/assets/images/FAQ-2.png'),
    iconName: "home",
    url: "https://rosatom.ru/career/sotrudnikam/kadrovaya-politika/"
  },
  {
    key: "3",
    title: "Что умеет чат-бот",
    description: "Короткое описание о чем-то",
    img: require('~/assets/images/FAQ-3.png'),
    iconName: "home",
    url: "https://www.rosatom.ru/sustainability/"
  }
]

let CHAT_ROOMS = [
  {
    key: "1",
    title: "Стажеры",
    name: "juniors",
    avatar: require("~/assets/images/chat_3.png"),
    ws: new WebSocket("wss://digital.spmi.ru/case/chatroom/juniors")
  },
  {
    key: "2",
    title: "Ментор/Стажер",
    name: "internship",
    avatar: require("~/assets/images/chat_1.png"),
    ws: new WebSocket("wss://digital.spmi.ru/case/chatroom/internship")
  },
  // {
  //   key: "3",
  //   title: "Менторы",
  //   name: "mentors",
  //   avatar: require("~/assets/images/chat_4.png"),
  //   ws: new WebSocket("wss://digital.spmi.ru/case/chatroom/mentors")
  // },
  {
    key: "3",
    title: "Чат-бот",
    name: "bot",
    avatar: require("~/assets/images/chat_2.png")
  }
]

const DEFAULT_CHAT_ROOMS = CHAT_ROOMS

const default_chat = require("~/assets/images/chat_default.png")

export default function (props) {
  const navigation = useNavigation()
  const state = useSelector((state) => state.auth)
  const theme = useSelector((state) => state.theme)

  const dispatch = useDispatch()
  const isFocused = useIsFocused()

  const connected = useNetInfo()

  const checkUserChats = () => {
    if(state.user){
      CHAT_ROOMS = DEFAULT_CHAT_ROOMS
      if(state.user) CHAT_ROOMS[2].ws = new WebSocket(`wss://digital.spmi.ru/case/chatroom/bot-${state.user.login}`)
      if(state.user.is_mentor) {
        CHAT_ROOMS[0].key = "-1"
        CHAT_ROOMS[1].title = "Мой стажер"
      }
      if(!state.user.is_mentor) {
        CHAT_ROOMS[1].title = "Мой ментор"
      }
    }
  }

  const renderTopStory = ({ item, index, separators }) => {
    return (
      <MenuButton 
        title={item.title}
        img={item.img}
        navigation={navigation} 
        description={item.description}
        webURL={item.url}
        iconName={item.iconName}
      />
    )
  }

  const renderChatRoom = ({item}) => {
    if(item.key == "-1") return <View></View>

    return (
    <TouchableOpacity activeOpacity={0.3} onPress={() => navigation.navigate("Chat", {WS: item.ws, title: item.title, name: item.name} )}>
      <View style={styles.rowDataContainer}>
        <View style={styles.rowIconContainer}>
          <Image
            source={item.avatar ? item.avatar : default_chat}
            containerStyle={item.avatar ? { ...styles.imageContainer, ...commonShadow } : styles.imageContainer}
            resizeMethod='scale'
            resizeMode='cover'
            style={[styles.imageSize, styles.image]}
          />
        </View>
        <View style={styles.rowTextContainer}>
          <Text style={styles.rowText}>{item.title}</Text>
        </View>
      </View>
    </TouchableOpacity>
    )
  }

  useEffect(() => {
    checkUserChats()
  }, [state])

  useEffect(() => {
    checkUserChats()
  }, [])

  if (!connected) {
    return <NoInternet />
  }

  return (
    <SafeAreaView style={styles.container}>
      <ScrollView style={styles.scrollContainer}>
          <Text style={styles.askText}>Часто спрашивают</Text>
          <FlatList
            decelerationRate={"fast"}
            data={TOP_STROIES}
            showsVerticalScrollIndicator={false}
            showsHorizontalScrollIndicator={false}
            horizontal={true}
            renderItem={renderTopStory}
            contentContainerStyle={styles.topStoriesFlatListContainer}
            style={styles.topStoriesFlatListStyle}
          />
          <Text style={styles.dialogText}>Диалоги</Text>
          {state.user && (<FlatList
            decelerationRate={"fast"}
            data={CHAT_ROOMS}
            showsVerticalScrollIndicator={false}
            showsHorizontalScrollIndicator={false}
            horizontal={false}
            renderItem={renderChatRoom}
            contentContainerStyle={styles.chatRoomFlatListContainer}
            style={styles.chatRoomFlatListStyle}
          />)}
           {!state.user && 
           (
           <View style={styles.loginContainer}>
             <Text style={styles.loginText}>Войдите, чтобы начать диалог</Text>
             {/* <Button
                containerStyle={styles.buttonContainerStyle}
                buttonStyle={{ ...styles.buttonStyle, backgroundColor: theme.mainColor }}
                disabledStyle={{ ...styles.buttonStyle, backgroundColor: theme.mainColor }}
                titleStyle={styles.buttonTitleStyle}
                onPress={() => navigation.navigate("Login")}
                iconContainerStyle={styles.iconContainer}
                title={"Войти"}
              /> */}
           </View>
           )
           }
      </ScrollView>
    </SafeAreaView>
  )
}

const styles = StyleSheet.create({
  container: {
    justifyContent: "center",
    paddingTop: isAndroid ? 30 : 0,
    minHeight: SCREEN_HEIGHT
  },
  scrollContainer: {
  },
  askText: {
    paddingLeft: 20,
    fontWeight: "600",
    fontSize: 22
  },
  dialogText: {
    marginTop: 20,
    paddingLeft: 20,
    fontWeight: "600",
    fontSize: 22
  },
  topStoriesFlatListStyle: {
  },
  topStoriesFlatListContainer: {
    marginTop: 20,
    alignItems: "center",
  },
  chatRoomFlatListContainer: { },
  chatRoomFlatListStyle: { },
  rowDataContainer: {
    width: SCREEN_WIDTH,
    height: 100,
    alignItems: "center",
    justifyContent: "flex-start",
    flexDirection: "row",
    borderBottomWidth: 0.3,
    borderBottomColor: "lightgray"
  },
  rowIconContainer: { 
    alignSelf: "center",
    paddingLeft: 10
  },
  rowTextContainer: { 
    paddingLeft: 15
  },
  rowText: {
    fontWeight: "500", 
    fontSize: 15 
  },
  imageSize: {
    width: 0.12 * SCREEN_WIDTH,
    height: 0.12 * SCREEN_WIDTH
  },
  imageContainer: {
    borderRadius: 40
  },
  image: { overflow: "hidden", borderRadius: 40 },
  buttonContainerStyle: {
    marginTop: 150 * fontSizeMult,
    alignSelf: "center",
    height: SCREEN_HEIGHT / (15 * fontSizeMult),
    width: SCREEN_WIDTH * 0.5
  },
  buttonStyle: {
    borderRadius: SCREEN_HEIGHT / 30,
    height: SCREEN_HEIGHT / (15 * fontSizeMult),
    width: SCREEN_WIDTH * 0.4
  },
  buttonTitleStyle: {
    fontSize: 18,
    color: "white",
    fontFamily: "light"
  },
  loginText: {
    paddingTop: 50,
    paddingLeft: 20,
    fontSize: 13,
    color: "black",
    fontFamily: "light"
  },
  loginContainer: {
      height: SCREEN_HEIGHT * 0.8,
      width: SCREEN_WIDTH
  }
})
