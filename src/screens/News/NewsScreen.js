import React, { useEffect, useState, useRef, useCallback } from "react"
import { Text, View, SafeAreaView, TouchableOpacity, StyleSheet, Platform, RefreshControl, Animated } from "react-native"

import { FlatList, TouchableWithoutFeedback } from "react-native-gesture-handler"
import { useSelector } from "react-redux"
import Constants from "expo-constants"
import { BlurView } from "expo-blur"

import { Placeholder, PlaceholderMedia, PlaceholderLine, Shine, Fade } from "rn-placeholder"
import { Image, ButtonGroup } from "react-native-elements"
import { cloneDeep } from "lodash"
import FontAwesome5 from "react-native-vector-icons/FontAwesome5"

import { useNetInfo } from "~/src/helpers/Connection"
import { NoInternet } from "~/src/components/NoInternet"
import { SCREEN_HEIGHT, SCREEN_WIDTH, FirstNews, fontSizeMult } from "~/src/CONSTANTS"
import theme, { commonShadow } from "~/src/components/theme.style"
import { fetchNews, viewPost } from "~/src/api/news"
import ServerCommonError from "~/src/components/ServerCommonError"
import WebviewModalize from "~/src/components/WebviewModalize/WebviewModalize"

const NEWS_HEIGHT = 280 // 280
const NEWS_PER_PAGE = 10 // NOTE: завязано на FirstNews
const FIRST_PAGE_INFO = {
  pageNumber: 0,
  point: 0, // точка для начала загрузки новой части Новостей
  loaded: true
}
const PADDING_HORIZONTAL = 10

const appTheme = theme.lightTheme
const default_image = require("~/assets/images/news-image-placeholder.png") // Заменить на стандартную фотку
const mining = require("~/assets/images/logo_rosatom.png") // placeholder for now

const postsTransformInit = {}
Array(10000)
  .fill("")
  .forEach((_, i) => {
    postsTransformInit[`${i}`] = new Animated.Value(1)
  })

export default function NewsScreen() {
  const connected = useNetInfo()
  const state = useSelector((state) => state.auth)
  const theme = useSelector((state) => state.theme)

  const buttons = ["События", "Новости"]
  const [selectedIndex, setSelectedIndex] = useState(0)

  const newsPagesInfo = useRef([FIRST_PAGE_INFO]).current
  const [news, setNews] = useState(FirstNews)
  const [refreshing, setRefreshing] = useState(null)
  const [newsError, setNewsError] = useState(null)
  const [postsTransform, setPostsTransform] = useState(postsTransformInit)
  const [newPostsLoading, setNewPostsLoading] = useState(null)

  const [pages, setPages] = useState(1)
  const [currentPage, setCurrentPage] = useState(1)

  const modalizeRef = useRef(null)
  const [modalURI, setModalURI] = useState(null)
  const flatListRef = useRef(null)

  const addPlaceholders = (page = 0) => {
    let newList = news
    for (let i = (page - 1) * NEWS_PER_PAGE; i < page * NEWS_PER_PAGE; i++) {
      newList.push({ key: i.toString(), loading: true })
    }
    setNews(newList)
    setNewPostsLoading(false)
  }

  const init = async (selectedButton = selectedIndex) => {
    setNewsError(null)
    setCurrentPage(0)
    const postsData = await getNewsFromServer(0, selectedButton, [])
    if (!postsData.error) {
      setPages(postsData.pages)
      // // set animations value array
      // const postsTransformInit = {}
      // Array(postsData.posts)
      //   .fill("")
      //   .forEach((_, i) => {
      //     postsTransformInit[`${i}`] = new Animated.Value(1)
      //   })
      // setPostsTransform(postsTransformInit)
      for (let i = 1; i < postsData.pages - 1; i++) {
        newsPagesInfo.push({
          pageNumber: i,
          point: i * NEWS_HEIGHT * NEWS_PER_PAGE - SCREEN_HEIGHT,
          loaded: false
        })
      }
      // setNewsPagesInfo(newsPagesInfo)
      setRefreshing(false)
    } else {
      setNews([])
      setNewsError(true)
    }
  }

  const getNewsFromServer = async (page = 0, selectedButton = selectedIndex, lastNews = news) => {
    let sources = selectedButton == 0 ? [4] : null
    let fetchedNews = await fetchNews(page + 1, NEWS_PER_PAGE, sources)
    let newsList = cloneDeep(lastNews)
    if (!fetchedNews.error && fetchedNews.posts.length) {
      fetchedNews.posts = fetchedNews.posts.map((x, i) => {
        return { ...x, key: x.id.toString() + x.title, loading: false }
      })
      newsList.splice(page * NEWS_PER_PAGE, (page + 1) * NEWS_PER_PAGE, ...fetchedNews.posts) // заменяем placeholder на новости
      setNews(newsList)
      return { pages: fetchedNews.total_pages, posts: fetchedNews.total_posts } // NOTE: Общее количество страниц с постами
    } else {
      //newsList.splice(page * NEWS_PER_PAGE, (page + 1) * NEWS_PER_PAGE, []) // убираем placeholder
      //setNews(newsList)
      return { error: "Error" } // с ошибкой
    }
  }

  useEffect(() => {
    init(selectedIndex)
  }, [])

  const onScroll = (event) => {
    const offsetY = event.nativeEvent.contentOffset.y
    let page = 0
    for (let i = 0; i < newsPagesInfo.length; i++) {
      if (newsPagesInfo[i].point - offsetY < (NEWS_HEIGHT * NEWS_PER_PAGE) / 3) {
        if (!newsPagesInfo[i].loaded) {
          setNewPostsLoading(true)
          page = newsPagesInfo[i].pageNumber
          setCurrentPage(page)
          if (page < pages) {
            newsPagesInfo[i].loaded = true
            addPlaceholders(page)
            getNewsFromServer(page)
            //}
          }
          break
        }
      }
      // Если в массиве такой элемент не нашелся, значит это конец ленты
    }
    // if (page) {
    //   let newInfo = newsPagesInfo.map((x) => (x.pageNumber == page ? { ...x, loaded: true } : x))
    //   setNewsPagesInfo(newInfo)
    // }
  }

  const pressInAnimate = (index) => {
    Animated.timing(postsTransform[index], {
      toValue: 0.9,
      duration: 250,
      useNativeDriver: true
    }).start()
  }

  const pressOutAnimate = (index) => {
    Animated.timing(postsTransform[index], {
      toValue: 1,
      duration: 250,
      useNativeDriver: true
    }).start()
  }

  const renderPlaceHolder = ({ item, index, separators }) => (
    <TouchableOpacity style={styles.itemContainer} activeOpacity={1} onPress={() => {}}>
      <View style={styles.itemImageContainer}>
        <Placeholder Animation={Fade}>
          <PlaceholderMedia style={styles.itemImagePlaceholder} />
        </Placeholder>
      </View>
      <View style={styles.itemInfoContainer}>
        {/* <View style={styles.itemDateContainer}>
          <Placeholder>
            <PlaceholderLine style={styles.itemDatePlaceholder} />
          </Placeholder>
        </View> */}
        <View style={styles.itemTextContainer}>
          <Placeholder Animation={Fade}>
            <PlaceholderMedia style={styles.itemTextPlaceholder} />
          </Placeholder>
        </View>
        <View style={styles.itemViewsContainer}>
          <Placeholder Animation={Fade}>
            <PlaceholderLine style={styles.itemViewsPlaceholder} />
          </Placeholder>
        </View>
      </View>
    </TouchableOpacity>
  )

  const renderItem = ({ item, index, separators }) => {
    // let options = {
    //   year: "numeric",
    //   month: "long",
    //   day: "numeric",
    //   weekday: "long",
    //   timezone: "UTC",
    // }
    // let date = new Date(item.pub_date).toLocaleString("ru-RU", options).toUpperCase()
    return (
      <TouchableWithoutFeedback
        containerStyle={styles.itemMainContainer}
        onPressIn={() => pressInAnimate(index)}
        onPressOut={() => pressOutAnimate(index)}
        onPress={async () => {
          let login = state.isLoggedIn ? state.user.login : Constants.deviceId
          viewPost(item.id, login) // + login
          setModalURI(item.url)
          modalizeRef.current.open()
        }}
      >
        <Animated.View style={{ ...styles.itemContainer, transform: [{ scale: postsTransform[index] }] }}>
          {/* <View style={styles.itemContainer}> */}
          <Image
            source={item.preview_img ? { uri: item.preview_img.url } : default_image}
            PlaceholderContent={
              <Placeholder Animation={Fade}>
                <PlaceholderMedia style={styles.itemImagePlaceholder} />
              </Placeholder>
            }
            containerStyle={styles.itemImageContainer}
            contentContainerStyle={styles.itemImageContentContainer}
            resizeMode={"cover"}
            // resizeMode={item.source.id == "2" && item.preview_img ? "contain" : "cover"}
            style={styles.image}
          />
          {item.source && (
            <BlurView tint='dark' intensity={60} style={{ ...styles.imageSourceContainer, ...styles.blurSourceContainer }}>
              <Image
                source={item.source.icon ? { uri: item.source.icon } : mining}
                placeholderStyle={styles.sourceImagePlaceholder}
                contentContainerStyle={styles.sourceImageContainer}
                style={styles.sourceImage}
              />
              <Text style={styles.sourceTitle}>{item.source.title}</Text>
            </BlurView>
          )}
          <View style={styles.itemInfoContainer}>
            {/* <View style={styles.itemDateContainer}>
            <Text style={styles.itemDateText}>{date}</Text>
          </View> */}
            <View style={styles.itemTextContainer}>
              <Text numberOfLines={3} style={styles.itemText}>
                {item.title}
              </Text>
            </View>
            <View style={styles.itemViewsContainer}>
              <FontAwesome5 name={"eye"} style={styles.itemViewsIcon} color={appTheme.tabBarOptions.inactiveTintColor} size={11} />
              <Text style={styles.itemViewsText}>{item.views}</Text>
            </View>
          </View>
        </Animated.View>
      </TouchableWithoutFeedback>
    )
  }

  const renderEmptyNews = () => {
    return (
      <View style={{ paddingVertical: 120 }}>
        <ServerCommonError customText='Постов еще нет' />
      </View>
    )
  }

  const updateNews = (selectedButton = selectedIndex) => {
    setRefreshing(true)
    setNews(FirstNews)
    newsPagesInfo.splice(0, pages - 1, FIRST_PAGE_INFO) // убираем placeholder
    init(selectedButton)
  }

  if (!connected) {
    return <NoInternet />
  }

  return (
    <SafeAreaView style={styles.container}>
      <FlatList
        ref={flatListRef}
        decelerationRate={"fast"}
        data={news}
        showsVerticalScrollIndicator={false}
        renderItem={(data) => (data.item.loading ? renderPlaceHolder(data) : renderItem(data))}
        contentContainerStyle={styles.flatListContainer}
        style={[styles.flatListStyle, styles.newsContainer]}
        onScroll={onScroll}
        refreshControl={<RefreshControl refreshing={refreshing} onRefresh={() => updateNews(selectedIndex)} />}
        ListEmptyComponent={renderEmptyNews}
      />
      {/* <ActivityIndicator
            animating={newPostsLoading}
            size={"large"}
            style={{
              top: SCREEN_HEIGHT - 100,
              position: "absolute",
              alignSelf: "center",
              zIndex: 2,
            }}
          /> */}

      <WebviewModalize modalizeRef={modalizeRef} modalURI={modalURI} modalHeight={0.84 * SCREEN_HEIGHT} />
    </SafeAreaView>
  )
}

const styles = StyleSheet.create({
  container: {
    marginTop: 0, // NOTE: hz,  isAndroid ? 20 : 0
    flexDirection: "column"
  },
  headerContainer: {
    paddingHorizontal: PADDING_HORIZONTAL,
    paddingBottom: 0
    // borderBottomWidth: 0.4,
    // borderBottomColor: appTheme.tabBarOptions.activeTintColor,
  },
  headerText: {
    // color: appTheme.tabBarOptions.activeTintColor,
    left: 10,
    top: 5,
    marginBottom: 5,
    fontSize: 30,
    fontWeight: "500"
  },
  buttonGroupContainer: { height: 30, borderWidth: 0.5, borderRadius: 8 },
  buttonInnerBorderStyle: { width: 0 },
  buttonStyle: { backgroundColor: appTheme.tabBarOptions.activeTintColor },
  newsContainer: {
    paddingBottom: 180
  },
  flatListStyle: {
    paddingTop: 5
  },
  flatListContainer: {
    alignItems: "center",
    paddingBottom: 150
  },
  itemMainContainer: {},
  itemContainer: {
    ...commonShadow,
    backgroundColor: "rgba(255, 255, 255, 1)",
    marginHorizontal: PADDING_HORIZONTAL,
    height: NEWS_HEIGHT,
    borderRadius: 10,
    marginVertical: 10
  },
  itemImageContainer: {
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,
    overflow: "hidden",
    height: 180,
    width: SCREEN_WIDTH - 2 * PADDING_HORIZONTAL
  },
  image: {
    height: 180,
    width: SCREEN_WIDTH - 2 * PADDING_HORIZONTAL
  },
  itemImagePlaceholder: {
    borderTopLeftRadius: 10, // NOTE: Доделать !
    borderTopRightRadius: 10,
    overflow: "hidden",
    height: 180,
    width: SCREEN_WIDTH - 2 * PADDING_HORIZONTAL
  },
  itemInfoContainer: {
    height: 180,
    width: SCREEN_WIDTH - 2 * PADDING_HORIZONTAL,
    paddingHorizontal: 15
  },
  itemDateContainer: {
    flexDirection: "row",
    paddingTop: 5,
    width: SCREEN_WIDTH * 0.8
  },
  itemDateText: {
    fontSize: 14,
    fontWeight: "400",
    color: appTheme.tabBarOptions.inactiveTintColor
  },
  itemDatePlaceholder: {
    width: SCREEN_WIDTH * 0.6,
    height: 14
  },
  itemTextContainer: {
    height: 80,
    justifyContent: "center"
  },
  itemText: {
    fontSize: 15,
    fontWeight: "500"
  },
  itemTextPlaceholder: {
    width: SCREEN_WIDTH - 2 * PADDING_HORIZONTAL - 40,
    height: 50
  },
  imageSourceContainer: {
    position: "absolute",
    flexDirection: "row",
    alignItems: "center",
    bottom: 100
  },
  blurSourceContainer: {
    width: SCREEN_WIDTH - 2 * PADDING_HORIZONTAL,
    height: 24,
    zIndex: 2
  },
  sourceImageContainer: {
    height: 25,
    width: 25,
    flexDirection: "row",
    left: 10,
    paddingLeft: 5,
    zIndex: 3
  },
  sourceTitle: {
    fontSize: 12,
    color: "white",
    paddingLeft: 5,
    fontWeight: "400"
  },
  sourceImage: {
    height: 25,
    width: 30,
    transform: [{scale: 0.8}]
  },
  sourceImagePlaceholder: {
    opacity: 0
  },
  itemViewsContainer: {
    flexDirection: "row",
    justifyContent: "flex-end",
    alignItems: "center"
  },
  itemViewsText: {
    paddingLeft: 6,
    color: appTheme.tabBarOptions.inactiveTintColor,
    fontSize: 10
  },
  itemViewsIcon: {},
  itemViewsPlaceholder: {
    left: SCREEN_WIDTH * 0.6,
    height: 12,
    width: 60
  },
  newsErrorContainer: {
    width: SCREEN_WIDTH,
    height: SCREEN_HEIGHT,
    alignItems: "center",
    justifyContent: "center"
  },
  newsErrorText: {
    fontSize: 14 * fontSizeMult,
    paddingTop: 5,
    color: appTheme.tabBarOptions.inactiveTintColor,
    textAlign: "center"
  }
})
