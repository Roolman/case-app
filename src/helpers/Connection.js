import { useState, useEffect } from "react"
import NetInfo from "@react-native-community/netinfo"

export function useNetInfo() {
  const [isConnected, setConnected] = useState(true)

  const onChange = (newState) => {
    setConnected(newState.isConnected)
  }

  useEffect(() => {
    NetInfo.fetch().then((connectionInfo) => {
      setConnected(
        connectionInfo.isConnected)
    })

    const unsubscribe = NetInfo.addEventListener(onChange)

    return () => {
      unsubscribe()
    }
  }, [])

  return isConnected
}
