export const hash = (...args) => {
  return [].join.call(args)
}

export const cachingDecorator = (func, hash, cache, setCache) => {
  async function wrapper() {
    let key = hash(...arguments)
    if (cache.has(key)) {
      return cache.get(key)
    }

    let result = await func.call(this, ...arguments)
    cache.set(key, result)
    setCache(cache)

    return result
  }

  return wrapper
}
