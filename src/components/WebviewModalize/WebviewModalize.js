import React, { useState, useRef, useCallback } from "react"
import { View, Text, Image, StyleSheet, Dimensions, Platform, TouchableOpacity, Share, Linking } from "react-native"

import Animated, { Easing } from "react-native-reanimated"
import FontAwesome5 from "react-native-vector-icons/FontAwesome5"
import { Modalize } from "react-native-modalize"
import { WebView } from "react-native-webview"
import Modal from "react-native-modal"

import { SCREEN_HEIGHT, SCREEN_WIDTH } from "~/src/CONSTANTS"
import theme, { commonShadow } from "~/src/components/theme.style"

const appTheme = theme.lightTheme
const { width, height: initialHeight } = Dimensions.get("window")
const isAndroid = Platform.OS === "android"

const extractHostname = (url) => {
  let hostname

  if (url.indexOf("//") > -1) {
    hostname = url.split("/")[2]
  } else {
    hostname = url.split("/")[0]
  }

  hostname = hostname.split(":")[0]
  hostname = hostname.split("?")[0]

  return hostname
}

const documentHeightCallbackScript = `
  function onElementHeightChange(elm, callback) {
    var lastHeight;
    var newHeight;
    (function run() {
      newHeight = Math.max(elm.clientHeight, elm.scrollHeight);
      if (lastHeight != newHeight) {
        callback(newHeight);
      }
      lastHeight = newHeight;
      if (elm.onElementHeightChangeTimer) {
        clearTimeout(elm.onElementHeightChangeTimer);
      }
      elm.onElementHeightChangeTimer = setTimeout(run, 200);
    })();
  }
  onElementHeightChange(document.body, function (height) {
    window.ReactNativeWebView.postMessage(
      JSON.stringify({
        event: 'documentHeight',
        documentHeight: height,
      }),
    );
  });
`

/* const documentHeightCallbackScript = `
  setTimeout(function() { 
    window.postMessage(document.documentElement.scrollHeight); 
  }, 500);
  true; // note: this is required, or you'll sometimes get silent failures
` */

export default function WebiewModalize(params) {
  const modalizeRef = params.modalizeRef
  const modalURI = params.modalURI
  const modalHeight = params.modalHeight ? params.modalHeight : 0.75 * SCREEN_HEIGHT
  const onCloseFunc = params.onClose
  const webViewRef = useRef(null)
  const [dotsMenuVisible, setDotsMenuVisible] = useState(false)

  const [url, setUrl] = useState("")
  const [secured, setSecure] = useState(true)
  const [mounted, setMounted] = useState(false)
  const [back, setBack] = useState(false)
  const [forward, setForward] = useState(false)
  const progress = useRef(new Animated.Value(0)).current
  const [layoutHeight, setLayoutHeight] = useState(initialHeight)
  const [documentHeight, setDocumentHeight] = useState(initialHeight)
  const height = isAndroid ? documentHeight : layoutHeight
  const [pageLoaded, setPageLoaded] = useState(false)

  const handleClose = () => {
    if (modalizeRef.current) {
      modalizeRef.current.close()
    }
  }

  const handleLoad = (status) => {
    setMounted(true)

    if (status === "progress" && !mounted) {
      return
    }

    const toValue = status === "start" ? 0.2 : status === "progress" ? 0.5 : status === "end" ? 2 : 0.5

    if (status != "end") {
      pageLoaded ? setPageLoaded(false) : ""
    }

    if (status === "progress") {
      setTimeout(() => {
        handleLoad("end")
      }, 3000)
    }

    if (status === "end") {
      !pageLoaded ? setPageLoaded(true) : ""
    }

    Animated.timing(progress, {
      toValue: toValue,
      duration: 200,
      easing: Easing.ease,
      useNativeDriver: true
    }).start()

    // if (status === "end") {
    //   Animated.timing(progress, {
    //     toValue: 2,
    //     duration: 200,
    //     easing: Easing.ease,
    //     useNativeDriver: true
    //   }).start()
    // }
    // () => {
    //   progress.setValue(0)
    // }
  }

  const handleNavigationStateChange = useCallback(({ url, canGoBack, canGoForward, loading, navigationType }) => {
    setBack(canGoBack)
    setForward(canGoForward)
    setSecure(url.includes("https"))
    setUrl(extractHostname(url))

    if (!loading && !navigationType && isAndroid) {
      if (webViewRef.current) {
        webViewRef.current.injectJavaScript(documentHeightCallbackScript)
      }
    }
  }, [])

  const handleMessage = useCallback((event) => {
    // iOS already inherit from the whole document body height,
    // so we don't have to manually get it with the injected script
    if (!isAndroid) {
      return
    }

    const data = JSON.parse(event.nativeEvent.data)

    if (!data) {
      return
    }

    switch (data.event) {
      case "documentHeight": {
        if (data.documentHeight !== 0) {
          console.log("data.documentHeight", data.documentHeight)
          if (data.documentHeight < 50000) {
            setDocumentHeight(data.documentHeight)
          }
        }

        break
      }
    }
  }, [])

  const handleBack = () => {
    if (webViewRef.current) {
      webViewRef.current.goBack()
    }
  }

  const handleForward = () => {
    if (webViewRef.current) {
      webViewRef.current.goForward()
    }
  }

  const handleLayout = ({ layout }) => {
    console.log("layout.height", layout.height)
    setLayoutHeight(layout.height)
  }

  const renderHeader = () => (
    <View style={modelHeaderStyles.header}>
      <View style={modelHeaderStyles.header__wrapper}>
        {/* <TouchableOpacity style={modelHeaderStyles.header__close} onPress={handleClose} activeOpacity={0.75}>
          <Image source={require("../../../assets/images/modal/cross.png")} />
        </TouchableOpacity> */}

        <TouchableOpacity
          style={{ opacity: back ? 0.7 : 0.2, height: 40, justifyContent: "center" }}
          onPress={handleBack}
          disabled={!back}
          activeOpacity={0.75}
        >
          <Image source={require("~/assets/images/modal/arrow.png")} />
        </TouchableOpacity>

        <View style={modelHeaderStyles.header__center}>
          {secured && <Image style={{ tintColor: "#31a14c" }} source={require("~/assets/images/modal/lock.png")} />}
          <Text style={[modelHeaderStyles.header__url, { color: secured ? "#31a14c" : "#5a6266" }]} numberOfLines={1}>
            {url}
          </Text>
        </View>

        <TouchableOpacity
          style={[modelHeaderStyles.header__arrowRight, { opacity: forward ? 1 : 0.2, height: 40, justifyContent: "center" }]}
          onPress={handleForward}
          disabled={!forward}
          activeOpacity={0.75}
        >
          <Image source={require("~/assets/images/modal/arrow.png")} />
        </TouchableOpacity>

        <TouchableOpacity
          style={{ ...modelHeaderStyles.header__dots, opacity: pageLoaded ? 1 : 0.2 }}
          disabled={!pageLoaded}
          onPress={() => setDotsMenuVisible(!dotsMenuVisible)}
        >
          <Image resizeMode='cover' source={require("~/assets/images/modal/dots.png")} />
        </TouchableOpacity>
      </View>

      <Animated.View
        style={[
          modelHeaderStyles.header__progress,
          {
            transform: [
              {
                translateX: progress.interpolate({
                  inputRange: [0, 0.2, 0.5, 1, 2],
                  outputRange: [-width, -width + 80, -width + 220, 0, 0]
                })
              }
            ],
            opacity: progress.interpolate({
              inputRange: [0, 0.1, 1, 2],
              outputRange: [0, 1, 1, 0]
            })
          }
        ]}
      />
    </View>
  )

  return (
    <Modalize
      ref={modalizeRef}
      HeaderComponent={renderHeader}
      scrollViewProps={{ showsVerticalScrollIndicator: false }}
      //avoidKeyboardLikeIOS={true}
      onLayout={handleLayout}
      onClose={isAndroid ? onCloseFunc : ""}
      modalHeight={modalHeight}
    >
      <WebView
        ref={webViewRef}
        source={{ uri: modalURI }}
        onLoadStart={() => handleLoad("start")}
        onLoadProgress={() => handleLoad("progress")}
        onLoadEnd={() => handleLoad("end")}
        onNavigationStateChange={handleNavigationStateChange}
        onMessage={handleMessage}
        // startInLoadingState={true}
        showsVerticalScrollIndicator={false}
        // androidHardwareAccelerationDisabled={true}
        scrollEnabled={!isAndroid}
        containerStyle={{ paddingBottom: 10 }}
        style={{ height }}
      />

      <Modal
        coverScreen={true}
        hasBackdrop={true}
        backdropOpacity={0.7}
        animationIn={"zoomIn"}
        animationInTiming={100}
        animationOut={"zoomOut"}
        animationOutTiming={100}
        onBackdropPress={() => {
          setDotsMenuVisible(false)
        }}
        isVisible={dotsMenuVisible}
        style={styles.dotsMenuModal}
      >
        <View style={styles.dotsMenuContainer}>
          <TouchableOpacity
            style={styles.dotsMenuLink}
            onPress={() => {
              Share.share(
                isAndroid
                  ? {
                      dialogTitle: modalURI,
                      message: modalURI
                    }
                  : {
                      url: modalURI,
                      excludedActivityTypes: ["com.apple.UIKit.activity.PostToTwitter"]
                    }
              )
            }}
          >
            <FontAwesome5 name={"share"} color={appTheme.tabBarOptions.inactiveTintColor} style={styles.dotsMenuIcon} size={16} />
            <Text style={styles.dotsMenuText}>Поделиться</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.dotsMenuLink}
            onPress={() => {
              setDotsMenuVisible(false)
              Linking.openURL(modalURI)
            }}
          >
            <FontAwesome5
              name={Platform.OS === "android" ? "chrome" : "compass"}
              color={appTheme.tabBarOptions.inactiveTintColor}
              style={styles.dotsMenuIcon}
              size={16}
            />
            <Text style={styles.dotsMenuText}>Открыть в браузере</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.dotsMenuLink}
            onPress={() => {
              setDotsMenuVisible(false)
              webViewRef.current.reload()
            }}
          >
            <FontAwesome5 name={"redo"} color={appTheme.tabBarOptions.inactiveTintColor} size={16} style={styles.dotsMenuIcon} />
            <Text style={styles.dotsMenuText}>Обновить</Text>
          </TouchableOpacity>
        </View>
      </Modal>
    </Modalize>
  )
}

const styles = StyleSheet.create({
  dotsMenuModal: {
    position: "absolute",
    right: 25,
    top: 0.2 * SCREEN_HEIGHT
  },
  dotsMenuContainer: {
    flexDirection: "column",
    width: 0.85 * SCREEN_WIDTH,
    backgroundColor: "white",
    borderRadius: 10,
    ...commonShadow
  },
  dotsMenuLink: {
    flexDirection: "row",
    alignItems: "center",
    padding: 20
  },
  dotsMenuIcon: { alignSelf: "center" },
  dotsMenuText: { paddingLeft: 20, fontSize: 18, fontWeight: "500", color: appTheme.tabBarOptions.inactiveTintColor }
})

const modelHeaderStyles = StyleSheet.create({
  // COPIED !!!
  header: {
    height: 44,

    borderBottomColor: "#c1c4c7",
    borderBottomWidth: 1,
    borderTopLeftRadius: 12,
    borderTopRightRadius: 12,
    overflow: "hidden"
  },

  header__wrapper: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
    zIndex: 3,
    paddingHorizontal: 12,

    height: "100%"
  },

  header__close: {},
  header__dots: { width: 30, height: 30, paddingTop: 13 },

  header__center: {
    flexDirection: "row",
    alignItems: "center"
  },

  header__url: {
    marginLeft: 4,
    fontSize: 16,
    fontWeight: "500"
  },

  header__arrowRight: {
    transform: [{ rotate: "180deg" }]
  },

  header__progress: {
    position: "absolute",
    top: 0,
    right: 0,
    bottom: 0,
    left: 0,

    backgroundColor: "#F3F3F3",

    opacity: 0,

    transform: [
      {
        translateX: -width
      }
    ]
  }
})
