import React from "react"
import { View, Text, StyleSheet } from "react-native"
import LottieView from "lottie-react-native"
import { SCREEN_HEIGHT, SCREEN_WIDTH } from "../CONSTANTS"
import theme from "./theme.style"

export default function ServerCommonError(props) {
  let width = props && props.width ? props.width : null
  let height = props && props.height ? props.height : null
  let customText = props && props.customText ? props.customText : null

  return (
    <View style={{ alignSelf: "center" }}>
      <LottieView
        resizeMode='contain'
        style={{
          height: height ? height : SCREEN_WIDTH * 0.9,
          width: width ? width : SCREEN_WIDTH * 0.9
        }}
        source={require("../animations/appErrorCoffee_Bodymovin.json")}
        autoPlay
        loop={true}
      />
      {!customText && <Text style={styles.emptyComponentText}>Сервис не доступен</Text>}
      {customText && <Text style={styles.emptyComponentText}>{customText}</Text>}
    </View>
  )
}

const styles = StyleSheet.create({
  emptyComponentText: {
    fontSize: 14,
    color: theme.lightTheme.tabBarOptions.inactiveTintColor,
    textAlign: "center"
  }
})
