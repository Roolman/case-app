import React, { useState, useEffect } from "react"
import { Text, View, StyleSheet } from "react-native"
import { TouchableOpacity } from "react-native-gesture-handler"
import LottieView from "lottie-react-native"
import { useSelector } from "react-redux"

import theme, { commonShadow } from "~/src/components/theme.style"
import { fontSizeMult, SCREEN_WIDTH, SCREEN_HEIGHT } from "~/src/CONSTANTS"

const appTheme = theme.lightTheme

export const NoInternet = () => {
  const theme = useSelector((state) => state.theme)

  const [loading, setLoading] = useState(null)

  useEffect(() => {
    return () => {
      clearTimeout(reTry)
    }
  }, [])

  const reTry = () => {
    setLoading(true)
    return setTimeout(() => {
      setLoading(false)
    }, 2000)
  }

  return (
    <View style={styles.container}>
      <LottieView style={styles.lottie} source={require("../animations/noConnection2_Bodymovin.json")} autoPlay loop={true} />
      <Text style={styles.text}>Нет подключения к Интернету</Text>
      <TouchableOpacity style={{ ...styles.repeatButton, backgroundColor: theme.mainColor }} disabled={loading} onPress={() => reTry()}>
        {!loading && <Text style={styles.repeatText}>Повторить</Text>}
        {loading && <LottieView style={styles.repeatAnimation} source={require("../animations/wifi_white.json")} autoPlay loop={true} />}
      </TouchableOpacity>
    </View>
  )
}

const styles = StyleSheet.create({
  container: { flex: 1, justifyContent: "center", alignItems: "center" },
  lottie: {
    height: 0.95 * SCREEN_WIDTH,
    width: 0.95 * SCREEN_WIDTH
  },
  text: { fontSize: 18, color: appTheme.tabBarOptions.inactiveTintColor },
  repeatButton: {
    alignItems: "center",
    justifyContent: "center",
    marginTop: 50,
    // backgroundColor: appTheme.tabBarOptions.activeTintColor,
    height: SCREEN_HEIGHT / 15,
    borderRadius: SCREEN_HEIGHT / 30,
    width: SCREEN_WIDTH * 0.5,
    ...commonShadow
  },
  repeatText: {
    fontSize: 18,
    color: "white",
    fontFamily: "light"
  },
  repeatAnimation: {
    height: 25 * fontSizeMult,
    width: 25 * fontSizeMult
  }
})
