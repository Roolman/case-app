import React, { Component } from "react"
import { View, Text, StyleSheet } from "react-native"
import { useRoute } from "@react-navigation/native"
import { useSelector } from "react-redux"

import FontAwesome5 from "react-native-vector-icons/FontAwesome5"
import { TouchableOpacity } from "react-native-gesture-handler"
import { Placeholder, PlaceholderLine, Shine } from "rn-placeholder"

import theme, { commonShadow } from "./theme.style"
import { SCREEN_WIDTH, fontSizeMult } from "../CONSTANTS"

const appTheme = theme.lightTheme

export default function ProfileCard(params) {
  const theme = useSelector((state) => state.theme)
  const {
    title,
    iconName,
    mainInfo,
    mainInfoColor,
    mainInfoText,
    secondInfo,
    secondInfoColor,
    secondInfoText,
    loading,
    CustomBodyComponent,
    onPressFunc
  } = params

  return (
    <View style={styles.container}>
      <TouchableOpacity activeOpacity={0.7} onPress={onPressFunc} style={styles.touchContainer}>
        <View style={styles.headerIconTextContainer}>
          <FontAwesome5 name={iconName} color={theme.mainColor} size={22} />
          <Text style={{ ...styles.headerTitleText, color: theme.mainColor }}>{title}</Text>
        </View>
        <View>
          <FontAwesome5 name={"chevron-right"} color={theme.mainColor} size={22} />
        </View>
      </TouchableOpacity>
      {CustomBodyComponent ? (
        <View style={styles.customBodyContainer}>
          <CustomBodyComponent />
        </View>
      ) : (
        <View style={styles.bodyContainer}>
          <View style={styles.mainInfoContainer}>
            <View>
              {!loading && <Text style={{ ...styles.mainInfoData, color: mainInfoColor }}>{mainInfo}</Text>}
              {loading && (
                <Placeholder>
                  <PlaceholderLine style={styles.mainInfoPlaceholder} />
                </Placeholder>
              )}
            </View>
            <View>
              {!loading && <Text style={styles.mainInfoText}>{mainInfoText}</Text>}
              {loading && (
                <Placeholder >
                  <PlaceholderLine style={styles.mainInfoTextPlaceholder} />
                </Placeholder>
              )}
            </View>
          </View>
          <View style={styles.secondInfoContainer}>
            <View>
              {!loading && <Text style={{ ...styles.secondInfoData, color: secondInfoColor }}>{secondInfo}</Text>}
              {loading && (
                <Placeholder >
                  <PlaceholderLine style={styles.secondInfoDataPlaceholder} />
                </Placeholder>
              )}
            </View>
            <View>
              {!loading && <Text style={styles.secondInfoText}>{secondInfoText}</Text>}
              {loading && (
                <Placeholder>
                  <PlaceholderLine style={styles.secondInfoTextPlaceholder} />
                </Placeholder>
              )}
            </View>
          </View>
        </View>
      )}
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    width: 0.9 * SCREEN_WIDTH,
    marginBottom: 16,
    ...commonShadow,
    flexDirection: "column",
    backgroundColor: "white",
    borderRadius: 15
  },
  touchContainer: {
    flexDirection: "row",
    justifyContent: "space-between",
    paddingLeft: 10 * fontSizeMult,
    paddingRight: 10 * fontSizeMult,
    paddingTop: 8 * fontSizeMult,
    paddingBottom: 8 * fontSizeMult,
    borderBottomColor: "#C4C4C4",
    borderBottomWidth: 0.4
  },
  headerIconTextContainer: {
    flexDirection: "row",
    alignItems: "center"
  },
  headerTitleText: {
    fontWeight: "600",
    // color: appTheme.tabBarOptions.activeTintColor,
    paddingLeft: 10
  },
  customBodyContainer: {
    paddingLeft: 10 * fontSizeMult,
    paddingTop: 10 * fontSizeMult,
    paddingBottom: 10 * fontSizeMult,
    paddingRight: 10 * fontSizeMult
  },
  bodyContainer: {
    flexDirection: "column",
    paddingLeft: 10 * fontSizeMult,
    paddingTop: 10 * fontSizeMult,
    paddingBottom: 10 * fontSizeMult
  },
  mainInfoContainer: {
    flexDirection: "row",
    paddingBottom: 20 * fontSizeMult,
    alignItems: "center"
  },
  mainInfoData: {
    fontSize: 30,
    fontWeight: "600"
  },
  mainInfoText: { fontWeight: "normal", color: "#707070", paddingLeft: 5 * fontSizeMult },
  mainInfoPlaceholder: {
    width: 0.2 * SCREEN_WIDTH,
    height: 21.2
  },
  mainInfoTextPlaceholder: {
    marginLeft: 80 * fontSizeMult,
    width: 0.5 * SCREEN_WIDTH,
    height: 10 * fontSizeMult
  },
  secondInfoContainer: {
    flexDirection: "row",
    alignItems: "center"
  },
  secondInfoData: { fontSize: 20, fontWeight: "normal" },
  secondInfoDataPlaceholder: {
    width: 0.2 * SCREEN_WIDTH,
    height: 15 * fontSizeMult
  },
  secondInfoText: { fontWeight: "normal", color: "#707070", paddingLeft: 5 * fontSizeMult },
  secondInfoTextPlaceholder: {
    marginLeft: 80 * fontSizeMult,
    width: 0.5 * SCREEN_WIDTH,
    height: 10 * fontSizeMult
  }
})
