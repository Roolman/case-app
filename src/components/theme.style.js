import { Dimensions } from "react-native"

const SCREEN_WIDTH = Dimensions.get("window").width
const SCREEN_HEIGHT = Dimensions.get("window").height
const lightThemeHeaderColor = "#000000"
const lightThemeBackgroundColor = "#fdfffe"
const logoColor = "#457EFF"
export const appBackgroundColor = "#f8faff"

export const commonShadow = {
  shadowColor: "#000",
  shadowOffset: {
    width: 0,
    height: 1
  },
  shadowOpacity: 0.22,
  shadowRadius: 2.22,

  elevation: 3
}

export default {
  lightTheme: {
    headerText: {
      flex: 1,
      fontSize: 24,
      color: lightThemeHeaderColor,
      textAlign: "left",
      fontFamily: "regular"
    },

    searchInput: {
      backgroundColor: "#FFFFFF",
      width: SCREEN_WIDTH - 50,
      borderRadius: 10
    },

    mainMenuButtonIconColor: logoColor,
    mainMenuButton: {
      backgroundColor: lightThemeBackgroundColor,
      borderRadius: 32,
      margin: 5,
      marginBottom: 10,
      background: "red",

      ...commonShadow
    },

    documentButton: {
      //backgroundColor: lightThemeBackgroundColor,
      borderRadius: 10,
      marginBottom: 10,
      height: SCREEN_HEIGHT / 18 + 10, //6
      width: SCREEN_WIDTH / 2.5, //2

      shadowColor: "#000",
      shadowOffset: {
        width: 0,
        height: 1
      },
      shadowOpacity: 0.22,
      shadowRadius: 2.22,

      elevation: 3
    },

    searchSuggestionsButton: {
      backgroundColor: lightThemeBackgroundColor,
      justifyContent: "center",
      marginBottom: 10
    },
    tabTheme: {
      darkTheme: false,
      colors: {
        // primary: logoColor, // Вот здесь хз еще, от Лого зависит
        background: appBackgroundColor,
        card: lightThemeBackgroundColor,
        text: "#262626",
        border: appBackgroundColor
      }
    },

    tabBarOptions: {
      activeTintColor: logoColor, // и здесь
      inactiveTintColor: "#686868",
      showLabel: true,

      style: {
        shadowColor: "#000000",
        shadowOffset: { height: 5 },
        shadowOpacity: 0.75,
        shadowRadius: 5,
        elevation: 5,
        backgroundColor: lightThemeBackgroundColor,
        borderTopColor: lightThemeBackgroundColor
      }
    },

    loginInputContainerStyle: {
      alignSelf: "center",
      borderWidth: 1,
      borderColor: "transparent", // #F3F6F9
      backgroundColor: "white", //
      borderRadius: SCREEN_HEIGHT / 30,
      height: SCREEN_HEIGHT / 15,
      width: SCREEN_WIDTH - 100,
      shadowColor: "#000",
      ...commonShadow
    }
  }
}
