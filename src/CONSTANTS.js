import { Dimensions } from "react-native"

export const DIGITAL_BASE_URL = "https://digital.spmi.ru/"
export const APP_KEY = "42e5586e54eb492983bc18bc3f0219dd"

export const NEWS_URL = "https://digital.spmi.ru/news_case/" + "api/v1/feed"
export const FILES_URL = DIGITAL_BASE_URL + "case/files/"
export const PROFILE_API_URL = DIGITAL_BASE_URL + "case/users/me"

export const MESSAGES_URL = "https://digital.spmi.ru/case/chat/chatroom"

export const SCREEN_WIDTH = Dimensions.get("window").width
export const SCREEN_HEIGHT = Dimensions.get("window").height

export const COURSES_URL = DIGITAL_BASE_URL + "case/courses/"

export const fontSizeMult = SCREEN_HEIGHT <= 700 ? 1 : 1.3

export const types = {
  student: "Студент",
  aspirant: "Аспирант",
  преподаватель: "Преподаватель",
  сотрудник: "Сотрудник"
}

export const FirstNews = [
  {
    key: "0",
    loading: true
  },
  {
    key: "1",
    loading: true
  },
  {
    key: "2",
    loading: true
  },
  {
    key: "3",
    loading: true
  },
  {
    key: "4",
    loading: true
  },
  {
    key: "5",
    loading: true
  },
  {
    key: "6",
    loading: true
  },
  {
    key: "7",
    loading: true
  },
  {
    key: "8",
    loading: true
  },
  {
    key: "9",
    loading: true
  }
]