import { LOGGED_OUT, LOGGED_IN } from "../types"
import * as SecureStore from "expo-secure-store"

// CONFIG KEYS [Storage Keys]===================================
// export const TOKEN_KEY = 'token';
export const USER_KEY = "d1lpk4a2kist3oa456"
// export const keys = [TOKEN_KEY, USER_KEY];

// Load Auth state
export const loadAuthState = () => async (dispatch) => {
  try {
    //GET USER
    let userDataRaw = await SecureStore.getItemAsync(USER_KEY)
    const userData = JSON.parse(userDataRaw)
    if (userData !== null) {
      await dispatch({ type: LOGGED_IN, user: userData })
    }
    //else await handleLogout()
  } catch (error) {
    throw new Error(error)
  }
}

// Handle Login
export const handleLogin = (data) => async (dispatch) => {
  try {
    // Сохраняем юзера в SecureStorage
    const userData = {
      token: data.hash,
      ...data
    }
    await SecureStore.setItemAsync(USER_KEY, JSON.stringify(userData))
    // Сделать Logout на сервере, чтобы http-only кукис стерлись. Перенести в api
    // await fetch(AUTH_URL + "?logout=yes")
    // DISPATCH TO REDUCER
    dispatch({ type: LOGGED_IN, user: userData })
  } catch (error) {
    throw new Error(error)
  }
}

// Handle Logout
export const handleLogout = () => async (dispatch) => {
  try {
    //REMOVE DATA
    await SecureStore.deleteItemAsync(USER_KEY)

    //DISPATCH TO REDUCER
    dispatch({ type: LOGGED_OUT })
  } catch (error) {
    throw new Error(error)
  }
}

// //UPDATE USER LOCAL STORAGE DATA AND DISPATCH TO REDUCER
// const updateUser = async (user) => {
//     try {
//         await AsyncStorage.setItem(USER_KEY, JSON.stringify(user));
//         dispatch({type: LOGGED_IN, user}); //DISPATCH TO REDUCER
//     } catch (error) {
//         throw new Error(error);
//     }
// };
