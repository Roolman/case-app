import { THEME_LOAD, THEME_RESET, THEME_UPDATE } from "../types"
import { AsyncStorage } from "react-native"

// load
export const loadTheme = () => async (dispatch) => {
  try {
    let themeRaw = await AsyncStorage.getItem("theme") // get last members
    if (themeRaw) {
      const theme = JSON.parse(themeRaw)
      dispatch({ type: THEME_LOAD, payload: theme })
    } else {
      dispatch({ type: THEME_LOAD, payload: null })
    }
  } catch (err) {
    console.log(err)
  }
}

// update
export const updateTheme = (theme) => async (dispatch) => {
  try {
    let payload = theme
    await AsyncStorage.setItem("theme", JSON.stringify(payload))
    dispatch({ type: THEME_UPDATE, payload })
  } catch (err) {
    console.log(err)
  }
}

// reset to default
export const resetTheme = () => async (dispatch) => {
  try {
    await AsyncStorage.removeItem("theme")
    dispatch({ type: THEME_RESET, payload: null })
  } catch (err) {
    console.log(err)
  }
}
