import * as SQLite from "expo-sqlite"

const db = SQLite.openDatabase("schedule.db")

export class DB {
  static init() {
    return new Promise((resolve, reject) => {
      db.transaction((tx) => {
        tx.executeSql(
          "CREATE TABLE IF NOT EXISTS schedules (id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, name TEXT NOT NULL, data TEXT NOT NULL, type TEXT NOT NULL, notificIds TEXT, syllabusId TEXT NOT NULL)",
          [],
          resolve,
          (_, err) => reject(err)
        )
      })
    })
  }

  static load() {
    return new Promise((resolve, reject) => {
      db.transaction((tx) => {
        tx.executeSql(
          "SELECT * from schedules",
          [],
          (_, result) => resolve(result.rows._array),
          (_, err) => reject(err)
        )
      })
    })
  }

  static add({ name, data, type, syllabusId }) {
    return new Promise((resolve, reject) => {
      db.transaction((tx) => {
        tx.executeSql(
          "INSERT INTO schedules (name, data, type, syllabusId) VALUES (?, ?, ?, ?)",
          [name, data, type, syllabusId],
          (_, result) => resolve(result.insertId),
          (_, err) => reject(err)
        )
      })
    })
  }

  static delete(id) {
    return new Promise((resolve, reject) => {
      db.transaction((tx) => {
        tx.executeSql("DELETE FROM schedules WHERE id = ?", [id], resolve, (_, err) => reject(err))
      })
    })
  }

  static reset_db() {
    return new Promise((resolve, reject) => {
      db.transaction((tx) => {
        tx.executeSql("DROP TABLE IF EXISTS schedules", [], resolve, (_, err) => reject(err))
      })
    })
  }
}
