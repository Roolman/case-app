import { createStore, combineReducers, applyMiddleware } from "redux"
import thunk from "redux-thunk"
import { authReducer } from "./reducers/auth"
import { themeReducer } from "./reducers/theme"

const rootReducer = combineReducers({
  auth: authReducer,
  theme: themeReducer
})

export default createStore(rootReducer, applyMiddleware(thunk))
