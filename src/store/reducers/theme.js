import { THEME_LOAD, THEME_UPDATE, THEME_RESET } from "../types"

export const initialState = {
  mainColor: "#135d82"
}

export const themeReducer = (state = initialState, action) => {
  switch (action.type) {
    case THEME_LOAD:
      return { ...state, mainColor: action.payload.mainColor ? action.payload.mainColor : initialState.mainColor }
    case THEME_UPDATE:
      return { ...state, mainColor: action.payload.mainColor }
    case THEME_RESET:
      return initialState
    default:
      return state
  }
}
