import { LOGGED_IN, LOGGED_OUT } from "../types"

export const initialState = {
  isLoggedIn: false,
  user: null
}

//REDUCER
export const authReducer = (state = initialState, action) => {
  switch (action.type) {
    case LOGGED_IN: {
      const user = action.user

      return { ...state, isLoggedIn: true, user }
    }

    case LOGGED_OUT: {
      return initialState
    }

    default:
      return state
  }
}
