//Auth Action Types
export const LOGGED_IN = "LOGGED_IN"
export const LOGGED_OUT = "LOGGED_OUT"

//Schedule Action Types
export const SCHEDULE_LOAD = "SCHEDULE_LOAD"
export const SCHEDULE_ADD = "SCHEDULE_ADD"
export const SCHEDULE_DELETE = "SCHEDULE_DELETE"
export const SCHEDULE_CHECK = "SCHEDULE_CHECK"
export const ADD_NOTIFICATIONS = "ADD_NOTIFICATIONS"
export const DELETE_NOTIFICATIONS = "DELETE_NOTIFICATIONS"

//Contacts Action Types
export const CONTACTS_SAVE = "CONTACTS_SAVE"
export const CONTACTS_LOAD = "CONTACTS_LOAD"
export const CONTACTS_DELETE = "CONTACTS_DELETE"

//Contacts Action Types
export const THEME_LOAD = "THEME_LOAD"
export const THEME_UPDATE = "THEME_UPDATE"
export const THEME_RESET = "THEME_RESET"
