import React, { useState, useEffect } from "react"
import { StatusBar, LogBox, AsyncStorage } from "react-native"

import { NavigationContainer } from "@react-navigation/native"
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs"
import FontAwesome5 from "react-native-vector-icons/FontAwesome5"
import { useDispatch, useSelector } from "react-redux"

import { createStackNavigator, HeaderBackButton } from "@react-navigation/stack"

import ChatScreen from "~/src/screens/Chat/ChatScreen"
import Chat from "~/src/screens/Chat/Chat"

import theme from "~/src/components/theme.style"
import AppLoading from "./components/AppLoading"

import HomeStack from "./navigation/homeStack"
import LoginStack from "./navigation/loginStack"

import { cacheImages, cacheFonts } from "./helpers/AssetsCaching"
import vectorFonts from "./helpers/vector-fonts"

import NewsScreen from "./screens/News/NewsScreen.js"

import { loadAuthState, handleLogout } from "./store/actions/auth"
import { DB } from "./store/db"
import { loadTheme, resetTheme } from "./store/actions/theme"

LogBox.ignoreAllLogs(true)

const appTheme = theme.lightTheme

export const AppMain = () => {
  const [isReady, setIsReady] = useState(false)
  const theme = useSelector((state) => state.theme)

  const dispatch = useDispatch()

  useEffect(() => {
    dispatch(loadAuthState())
  }, [dispatch])

  const loadAssetsAsync = async () => {
    const imageAssets = cacheImages([
      require("../assets/images/holder.png"),
      require("../assets/images/calendar.png"),
      require("../assets/images/default_profile.png"),
      require("../assets/images/default_image.png"),
      require("../assets/images/trash.png"),
      require("../assets/images/no_user.png"),
      require("../assets/images/modal/arrow.png"),
      require("../assets/images/modal/dots.png"),
      require("../assets/images/modal/lock.png"),
      require("../assets/images/logo_rosatom.png"),
      require("../assets/images/story-1.png"),
      require("../assets/images/story-2.png"),
      require("../assets/images/story-3.png"),
      require("../assets/images/story-4.png"),
      require("../assets/images/story-5.png"),
      require("../assets/images/medal.png"),
      require("../assets/images/achievement.png"),
      require("../assets/images/goal.png"),
      require("../assets/images/trophy.png"),
      require("~/assets/images/chat_default.png"),
      require("~/assets/images/chat_1.png"),
      require("~/assets/images/chat_2.png"),
      require("~/assets/images/chat_3.png"),
      require("~/assets/images/chat_4.png"),
      require("~/assets/images/FAQ-1.png"),
      require("~/assets/images/FAQ-2.png"),
      require("~/assets/images/FAQ-3.png"),
    ])

    const fontAssets = cacheFonts([
      ...vectorFonts,
      { georgia: require("../assets/fonts/Georgia.ttf") },
      { regular: require("../assets/fonts/Montserrat-Regular.ttf") },
      { light: require("../assets/fonts/Montserrat-Light.ttf") },
      { bold: require("../assets/fonts/Montserrat-Bold.ttf") },
      { UbuntuLight: require("../assets/fonts/Ubuntu-Light.ttf") },
      { UbuntuBold: require("../assets/fonts/Ubuntu-Bold.ttf") },
      { UbuntuLightItalic: require("../assets/fonts/Ubuntu-Light-Italic.ttf") }
    ])

    await Promise.all([...imageAssets, ...fontAssets])

    // NOTE: only for prod
  }

  if (!isReady) {
    return <AppLoading startAsync={loadAssetsAsync} onFinish={() => setIsReady(true)} onError={console.warn} />
  }

  const Tab = createBottomTabNavigator()
  let tabTheme = appTheme.tabTheme
  let tabBarOptions = appTheme.tabBarOptions
  tabTheme.colors.primary = theme.mainColor
  tabBarOptions.activeTintColor = theme.mainColor

  const MainTabs = () => {
    return (
        <Tab.Navigator
          screenOptions={({ route }) => ({
            tabBarIcon: ({ color, size }) => {
              let iconName

              if (route.name === "Профиль") {
                iconName = "user-circle"
              } else if (route.name === "События") {
                iconName = "newspaper"
              } else if (route.name === "Чат") {
                iconName = "comments"
              } else if (route.name === "Главная") {
                iconName = "home"
              }

              return <FontAwesome5 name={iconName} size={size} color={color} />
            }
          })}
          tabBarOptions={appTheme.tabBarOptions}
        >
          <Tab.Screen name='Главная' component={HomeStack} />

          <Tab.Screen name='События' component={NewsScreen} />

          <Tab.Screen name='Чат' component={ChatScreen} />

          <Tab.Screen name='Профиль' component={LoginStack} />
        </Tab.Navigator>
    )
  } 

  const Stack = createStackNavigator()

  return (
    <NavigationContainer theme={tabTheme}>
      <StatusBar barStyle='dark-content' />
      <Stack.Navigator>
        <Stack.Screen name='Главная' component={MainTabs} options={{ headerShown: false }} />
        <Stack.Screen name='Chat' component={Chat} options={{ headerShown: true }} />
      </Stack.Navigator>
    </NavigationContainer>
  )
}
