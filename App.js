import React from "react"
import { AppMain } from "./src/AppMain"
import { Provider } from "react-redux"
import store from "./src/store"

export default () => {
  return (
    <Provider store={store}>
      <AppMain />
    </Provider>
  )
}
